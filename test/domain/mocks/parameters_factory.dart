import 'package:faker/faker.dart';

import 'package:clean_flutter_app_updated/domain/usecases/usecases.dart';

class ParametersFactory {
  static AddAccountParameters makeAddAccount() => AddAccountParameters(
    name: faker.person.name(),
    email: faker.internet.email(),
    password: faker.internet.password(),
    passwordConfirmation: faker.internet.password(),
  );

  static AuthenticationParameters makeAuthentication() =>
      AuthenticationParameters(
        email: faker.internet.email(),
        secret: faker.internet.password(),
      );
}
