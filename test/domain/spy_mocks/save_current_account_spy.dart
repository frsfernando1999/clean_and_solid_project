import 'package:mocktail/mocktail.dart';

import 'package:clean_flutter_app_updated/domain/helpers/helpers.dart';
import 'package:clean_flutter_app_updated/domain/usecases/usecases.dart';

class SaveCurrentAccountSpy extends Mock implements SaveCurrentAccount {
  SaveCurrentAccountSpy(){
    mockSaveCurrentAccount();
  }
  When mockSaveCurrentAccountCall() => when(() =>  save(any()));
  void mockSaveCurrentAccount() => mockSaveCurrentAccountCall().thenAnswer((_) async => _);
  void mockSaveCurrentAccountError() => mockSaveCurrentAccountCall().thenThrow(DomainError.unexpected);
}
