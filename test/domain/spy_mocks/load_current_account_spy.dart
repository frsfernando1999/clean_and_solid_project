import 'package:mocktail/mocktail.dart';

import 'package:clean_flutter_app_updated/domain/entities/entities.dart';
import 'package:clean_flutter_app_updated/domain/usecases/usecases.dart';

class LoadCurrentAccountSpy extends Mock implements LoadCurrentAccount {
  When mockLoadCall() => when(() =>  load());

  void mockLoad({required AccountEntity account}) =>
      mockLoadCall().thenAnswer((_) async => account);

  void mockLoadError() =>
      mockLoadCall().thenThrow(Exception());
}