import 'package:mocktail/mocktail.dart';

import 'package:clean_flutter_app_updated/domain/entities/entities.dart';
import 'package:clean_flutter_app_updated/domain/helpers/helpers.dart';
import 'package:clean_flutter_app_updated/domain/usecases/usecases.dart';

class AuthenticationSpy extends Mock implements Authentication {
  When mockAuthenticationCall() => when(() =>  auth(any()));
  void mockAuthentication(AccountEntity data) => mockAuthenticationCall().thenAnswer((_) async => data);
  void mockAuthenticationError(DomainError error) => mockAuthenticationCall().thenThrow(error);
}

