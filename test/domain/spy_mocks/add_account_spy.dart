import 'package:mocktail/mocktail.dart';

import 'package:clean_flutter_app_updated/domain/entities/entities.dart';
import 'package:clean_flutter_app_updated/domain/helpers/helpers.dart';
import 'package:clean_flutter_app_updated/domain/usecases/usecases.dart';

class AddAccountSpy extends Mock implements AddAccount {
  When mockAddAccountCall() => when(() =>  add(any()));
  void mockAddAccount(AccountEntity data) => mockAddAccountCall().thenAnswer((_) async => data);
  void mockAddAccountError(DomainError error) => mockAddAccountCall().thenThrow(error);
}
