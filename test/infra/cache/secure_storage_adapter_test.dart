import 'package:faker/faker.dart';
import 'package:mocktail/mocktail.dart';
import 'package:test/test.dart';

import 'package:clean_flutter_app_updated/infra/cache/cache.dart';

import '../spy_mocks/spy_mocks.dart';

//Spy - capturar valores e mockar resultados
//Stub - mockar o resultado apenas
//Mock - capturar valores apenas

void main() {
  late FlutterSecureStorageSpy secureStorage;
  late SecureStorageAdapter sut;
  late String key;
  late String value;

  setUp(() {
    key = faker.lorem.word();
    value = faker.guid.guid();
    secureStorage = FlutterSecureStorageSpy();
    secureStorage.mockFetch(value);
    sut = SecureStorageAdapter(secureStorage: secureStorage);
  });

  group('saveSecure', () {
    test('Should call save secure with correct values', () async {
      //act
      await sut.save(key: key, value: value);

      //assert
      verify(() => secureStorage.write(key: key, value: value));
    });

    test('Should throw if save secure throws', () async {
      //arrange
      secureStorage.mockSaveError();

      //act
      final future = sut.save(key: key, value: value);

      //assert
      expect(future, throwsA(const TypeMatcher<Exception>()));
    });
  });

  group('fetchSecure', () {
    test('Should call fetch secure with correct value', () async {
      //act
      await sut.fetch(key);
      //assert
      verify(() => secureStorage.read(key: key));
    });

    test('Should return correct value on success', () async {
      //act
      final fetchedValue = await sut.fetch(key);
      //assert
      expect(fetchedValue, value);
    });

    test('Should throw if fetch secure throws', () async {
      //arrange
      secureStorage.mockFetchError();
      //act
      final future = sut.fetch(key);
      //assert
      expect(future, throwsA(const TypeMatcher<Exception>()));
    });
  });

  group('delete', () {
    test('Should call delete with correct key', () async {
      await sut.delete(key);

      verify(() => secureStorage.delete(key: key)).called(1);
    });

    test('Should throw if delete throws', () async {
      secureStorage.mockDeleteError();

      final future = sut.delete(key);

      expect(future, throwsA(const TypeMatcher<Exception>()));
    });
  });
}
