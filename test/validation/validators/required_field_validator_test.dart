import 'package:test/test.dart';

import 'package:clean_flutter_app_updated/presentation/protocols/protocols.dart';
import 'package:clean_flutter_app_updated/validation/validators/validators.dart';

void main() {
  late RequiredFieldValidation sut;
  setUp(() {
    sut = const RequiredFieldValidation('any_field');
  });

  test('Should return null if value is not empty', () {
    expect(sut.validate({'any_field': 'any_value'}), null);
  });

  test('Should return error if value is empty', () {
      expect(sut.validate({'any_field': ''}), ValidationError.requiredField);
  });

  test('Should return error if value is null', () {
    expect(sut.validate({'any_field': null}), ValidationError.requiredField);
  });
}
