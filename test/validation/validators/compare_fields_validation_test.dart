import 'package:test/test.dart';

import 'package:clean_flutter_app_updated/presentation/protocols/validation.dart';
import 'package:clean_flutter_app_updated/validation/validators/validators.dart';

void main() {
  late CompareFieldsValidation sut;
  setUp(() {
    sut = const CompareFieldsValidation(
        field: 'any_field', fieldToCompare: 'other_field');
  });

  test('Should return null on invalid cases', () {
    expect(sut.validate({'any_field': 'any_value'}), null);
    expect(sut.validate({'other_field': 'any_value'}), null);
    expect(sut.validate({}), null);
  });

  test('Should return error if value is not equal', () {
    final formData = {
      'any_field': 'any_value',
      'other_field': 'other_value'};
    expect(sut.validate(formData), ValidationError.invalidField);
  });

  test('Should return null if values are equal', () {
    final formData = {
      'any_field': 'any_value',
      'other_field': 'any_value'};

    expect(sut.validate(formData), null);
  });
}
