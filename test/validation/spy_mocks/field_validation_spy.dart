import 'package:mocktail/mocktail.dart';

import 'package:clean_flutter_app_updated/presentation/protocols/protocols.dart';
import 'package:clean_flutter_app_updated/validation/protocols/protocols.dart';

class FieldValidationSpy extends Mock implements FieldValidation{
  FieldValidationSpy(){
    mockValidation();
    mockFieldName('any_field');
  }
  When mockValidationCall() => when(() =>  validate(any()));
  void mockValidation() => mockValidationCall().thenReturn(null);
  void mockValidationError(ValidationError error) => mockValidationCall().thenReturn(error);

  void mockFieldName(String fieldName) => when(() =>  field).thenReturn(fieldName);
}