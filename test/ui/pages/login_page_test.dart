import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

import 'package:faker/faker.dart';

import 'package:clean_flutter_app_updated/ui/helpers/i18n/i18n.dart';
import 'package:clean_flutter_app_updated/ui/helpers/errors/errors.dart';
import 'package:clean_flutter_app_updated/ui/pages/pages.dart';

import '../helpers/helpers.dart';
import '../spy_mocks/spy_mocks.dart';

void main() {
  late LoginPresenterSpy presenter;

  Future<void> loadPage(WidgetTester tester) async {
    presenter = LoginPresenterSpy();
    await tester.pumpWidget(
        makePage(path: '/login', page: () => LoginPage(presenter: presenter)));
  }

  tearDown(() {
    presenter.dispose();
  });

  testWidgets('Should call validate with correct value',
      (WidgetTester tester) async {
    //arrange
    await loadPage(tester);

    final email = faker.internet.email();
    final password = faker.internet.password();
    //act
    await tester.enterText(
        find.bySemanticsLabel(R.strings.loginEmailLabelText), email);
    await tester.enterText(
        find.bySemanticsLabel(R.strings.loginPasswordLabelText), password);
    //assert
    verify(() => presenter.validateEmail(email));
    verify(() => presenter.validatePassword(password));
  });

  testWidgets('Should present error if email is invalid',
      (WidgetTester tester) async {
    //arrange
    await loadPage(tester);

    //act
    presenter.emitEmailError(UIError.invalidField);
    await tester.pump();

    //assert
    expect(find.text(R.strings.invalidFieldError), findsOneWidget);
  });

  testWidgets('Should present error if email is empty',
      (WidgetTester tester) async {
    //arrange
    await loadPage(tester);

    //act
    presenter.emitEmailError(UIError.requiredField);
    await tester.pump();

    //assert
    expect(find.text(R.strings.requiredFieldError), findsOneWidget);
  });

  testWidgets('Should present no error if email is valid',
      (WidgetTester tester) async {
    //arrange
    await loadPage(tester);

    //act
    presenter.emitValidEmail();
    await tester.pump();

    //assert
    expect(
      find.descendant(
          of: find.bySemanticsLabel(R.strings.loginEmailLabelText),
          matching: find.byType(Text)),
      findsOneWidget,
    );
  });

  testWidgets('Should present error if password is invalid',
      (WidgetTester tester) async {
    //arrange
    await loadPage(tester);

    //act
    presenter.emitPasswordError(UIError.requiredField);
    await tester.pump();

    //assert
    expect(find.text(R.strings.requiredFieldError), findsOneWidget);
  });

  testWidgets('Should present no error if password is valid',
      (WidgetTester tester) async {
    //arrange
    await loadPage(tester);

    //act
    presenter.emitValidPassword();
    await tester.pump();

    //assert
    expect(
      find.descendant(
          of: find.bySemanticsLabel('Senha'), matching: find.byType(Text)),
      findsOneWidget,
    );
  });

  testWidgets('Should enable form button if form is valid',
      (WidgetTester tester) async {
    //arrange
    await loadPage(tester);

    //act
    presenter.emitValidForm();
    await tester.pump();

    //assert
    final button = tester.widget<ElevatedButton>(find.byType(ElevatedButton));
    expect(button.onPressed, isNotNull);
  });

  testWidgets('Should disable form button if form is invalid',
      (WidgetTester tester) async {
    //arrange
    await loadPage(tester);

    //act
    presenter.emitFormError();
    await tester.pump();

    //assert
    final button = tester.widget<ElevatedButton>(find.byType(ElevatedButton));
    expect(button.onPressed, null);
  });

  testWidgets('Should call authentication on form submit',
      (WidgetTester tester) async {
    //arrange
    await loadPage(tester);

    //act
    presenter.emitValidForm();
    await tester.pump();
    await tester.ensureVisible(find.byType(ElevatedButton));
    await tester.tap(find.byType(ElevatedButton));
    await tester.pump();
    //assert
    verify(() => presenter.auth()).called(1);
  });

  testWidgets('Should handle loading correctly', (WidgetTester tester) async {
    await loadPage(tester);

    presenter.emitLoading();
    await tester.pump();
    expect(find.byType(CircularProgressIndicator), findsOneWidget);

    presenter.emitLoading(false);
    await tester.pump();
    expect(find.byType(CircularProgressIndicator), findsNothing);

    presenter.emitLoading();
    await tester.pump();
    expect(find.byType(CircularProgressIndicator), findsOneWidget);
  });

  testWidgets('Should present error snackbar message if authentication fails',
      (WidgetTester tester) async {
    //arrange
    await loadPage(tester);

    //act
    presenter.emitMainError(UIError.invalidCredentials);
    await tester.pump();

    //assert
    expect(find.text(R.strings.credentialsError), findsOneWidget);
  });

  testWidgets('Should present error snackbar message if authentication throws',
      (WidgetTester tester) async {
    //arrange
    await loadPage(tester);

    //act
    presenter.emitMainError(UIError.unexpected);
    await tester.pump();

    //assert
    expect(find.text(R.strings.unexpectedError), findsOneWidget);
  });

  testWidgets('Should change page', (WidgetTester tester) async {
    //arrange
    await loadPage(tester);

    //act
    presenter.emitNavigateTo('/any_route');
    await tester.pumpAndSettle();

    //assert
    expect(currentRoute, '/any_route');
    expect(find.text('fake page'), findsOneWidget);
  });

  testWidgets('Should not change page', (WidgetTester tester) async {
    await loadPage(tester);

    presenter.emitNavigateTo('');
    await tester.pump();
    expect(currentRoute, '/login');
  });

  testWidgets('Should call goToSignUp on link click',
      (WidgetTester tester) async {
    //arrange
    await loadPage(tester);

    //act
    final button = find.text(R.strings.loginGoToCreateAccountButtonText);
    await tester.ensureVisible(button);
    await tester.tap(button);
    await tester.pump();
    //assert
    verify(() => presenter.goToSignUp()).called(1);
  });
}
