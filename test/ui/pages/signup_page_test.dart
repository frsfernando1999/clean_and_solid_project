import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:get/route_manager.dart';
import 'package:faker/faker.dart';
import 'package:mocktail/mocktail.dart';

import 'package:clean_flutter_app_updated/ui/helpers/helpers.dart';
import 'package:clean_flutter_app_updated/ui/pages/pages.dart';

import '../helpers/helpers.dart';
import '../spy_mocks/spy_mocks.dart';

void main() {
  late SignupPresenterSpy presenter;

  Future<void> loadPage(WidgetTester tester) async {
    presenter = SignupPresenterSpy();
    await tester.pumpWidget(makePage(path: '/signup', page: () => SignupPage(presenter: presenter)
    ));
  }

  tearDown(() {
    presenter.dispose();
  });

  testWidgets('Should call validate with correct value',
      (WidgetTester tester) async {
    //arrange
    await loadPage(tester);

    final name = faker.person.name();
    final email = faker.internet.email();
    final password = faker.internet.password();
    //act
    await tester.enterText(
        find.bySemanticsLabel(R.strings.signupNameLabelText), name);
    await tester.enterText(
        find.bySemanticsLabel(R.strings.signupEmailLabelText), email);
    await tester.enterText(
        find.bySemanticsLabel(R.strings.signupPasswordLabelText), password);
    await tester.enterText(
        find.bySemanticsLabel(R.strings.signupPasswordConfirmationLabelText),
        password);
    //assert
    verify(() => presenter.validateName(name));
    verify(() => presenter.validateEmail(email));
    verify(() => presenter.validatePassword(password));
    verify(() => presenter.validatePasswordConfirmation(password));
  });

  testWidgets('Should present email error', (WidgetTester tester) async {
    //arrange
    await loadPage(tester);
    //Test invalid field error
    presenter.emitEmailError(UIError.invalidField);
    await tester.pump();
    expect(find.text(R.strings.invalidFieldError), findsOneWidget);

    //Test required field error
    presenter.emitEmailError(UIError.requiredField);
    await tester.pump();
    expect(find.text(R.strings.requiredFieldError), findsOneWidget);

    //Test no errors
    presenter.emitValidEmail();
    await tester.pump();
    expect(
      find.descendant(
          of: find.bySemanticsLabel(R.strings.signupEmailLabelText),
          matching: find.byType(Text)),
      findsOneWidget,
    );
  });

  testWidgets('Should present name error', (WidgetTester tester) async {
    //arrange
    await loadPage(tester);
    //Test invalid field error
    presenter.emitNameError(UIError.invalidField);
    await tester.pump();
    expect(find.text(R.strings.invalidFieldError), findsOneWidget);

    //Test required field error
    presenter.emitNameError(UIError.requiredField);
    await tester.pump();
    expect(find.text(R.strings.requiredFieldError), findsOneWidget);

    //Test no errors
    presenter.emitValidName();
    await tester.pump();
    expect(
      find.descendant(
          of: find.bySemanticsLabel(R.strings.signupNameLabelText),
          matching: find.byType(Text)),
      findsOneWidget,
    );
  });

  testWidgets('Should present password error', (WidgetTester tester) async {
    //arrange
    await loadPage(tester);
    //Test invalid field error
    presenter.emitPasswordError(UIError.invalidField);
    await tester.pump();
    expect(find.text(R.strings.invalidFieldError), findsOneWidget);

    //Test required field error
    presenter.emitPasswordError(UIError.requiredField);
    await tester.pump();
    expect(find.text(R.strings.requiredFieldError), findsOneWidget);

    //Test no errors
    presenter.emitValidPassword();
    await tester.pump();
    expect(
      find.descendant(
          of: find.bySemanticsLabel(R.strings.signupPasswordLabelText),
          matching: find.byType(Text)),
      findsOneWidget,
    );
  });

  testWidgets('Should present password confirmation error', (WidgetTester tester) async {
    //arrange
    await loadPage(tester);
    //Test invalid field error
    presenter.emitPasswordConfirmationError(UIError.invalidField);
    await tester.pump();
    expect(find.text(R.strings.invalidFieldError), findsOneWidget);

    //Test required field error
    presenter.emitPasswordConfirmationError(UIError.requiredField);
    await tester.pump();
    expect(find.text(R.strings.requiredFieldError), findsOneWidget);

    //Test no errors
    presenter.emitValidPasswordConfirmation();
    await tester.pump();
    expect(
      find.descendant(
          of: find.bySemanticsLabel(R.strings.signupPasswordConfirmationLabelText),
          matching: find.byType(Text)),
      findsOneWidget,
    );
  });

  testWidgets('Should enable form button if form is valid',
          (WidgetTester tester) async {
        //arrange
        await loadPage(tester);

        //act
        presenter.emitValidForm();
        await tester.pump();

        //assert
        final button = tester.widget<ElevatedButton>(find.byType(ElevatedButton));
        expect(button.onPressed, isNotNull);
      });

  testWidgets('Should disable form button if form is invalid',
          (WidgetTester tester) async {
        //arrange
        await loadPage(tester);

        //act
        presenter.emitFormError();
        await tester.pump();

        //assert
        final button = tester.widget<ElevatedButton>(find.byType(ElevatedButton));
        expect(button.onPressed, null);
      });

  testWidgets('Should call signup on form submit',
          (WidgetTester tester) async {
        //arrange
        await loadPage(tester);

        //act
        presenter.emitValidForm();
        await tester.pump();
        await tester.ensureVisible(find.byType(ElevatedButton));
        await tester.tap(find.byType(ElevatedButton));
        await tester.pump();
        //assert
        verify(() => presenter.signup()).called(1);
      });

  testWidgets('Should handle loading correctly', (WidgetTester tester) async {
    await loadPage(tester);

    presenter.emitLoading();
    await tester.pump();
    expect(find.byType(CircularProgressIndicator), findsOneWidget);

    presenter.emitLoading(false);
    await tester.pump();
    expect(find.byType(CircularProgressIndicator), findsNothing);

    presenter.emitLoading();
    await tester.pump();
    expect(find.byType(CircularProgressIndicator), findsOneWidget);
  });


  testWidgets('Should present error snackbar message if signup fails',
          (WidgetTester tester) async {
        //arrange
        await loadPage(tester);

        //act
        presenter.emitMainError(UIError.emailInUse);
        await tester.pump();

        //assert
        expect(find.text(R.strings.emailInUseError), findsOneWidget);
      });

  testWidgets('Should present error snackbar message if signup throws',
          (WidgetTester tester) async {
        //arrange
        await loadPage(tester);

        //act
        presenter.emitMainError(UIError.unexpected);
        await tester.pump();

        //assert
        expect(find.text(R.strings.unexpectedError), findsOneWidget);
      });


  testWidgets('Should change page',
          (WidgetTester tester) async {
        //arrange
        await loadPage(tester);

        //act
        presenter.emitNavigateTo('/any_route');
        await tester.pumpAndSettle();

        //assert
        expect(Get.currentRoute, '/any_route');
        expect(find.text('fake page'), findsOneWidget);
      });

  testWidgets('Should not change page', (WidgetTester tester) async {
    await loadPage(tester);

    presenter.emitNavigateTo('');
    await tester.pump();
    expect(Get.currentRoute, '/signup');

  });

  testWidgets('Should call goToLogin on link click',
          (WidgetTester tester) async {
        //arrange
        await loadPage(tester);

        //act
        final button = find.text(R.strings.signupGoToLoginPageTextButton);
        await tester.ensureVisible(button);
        await tester.tap(button);
        await tester.pump();
        //assert
        verify(() => presenter.goToLogin()).called(1);
      });


}
