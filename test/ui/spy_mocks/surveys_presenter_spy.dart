import 'dart:async';

import 'package:mocktail/mocktail.dart';

import 'package:clean_flutter_app_updated/ui/pages/pages.dart';

class SurveysPresenterSpy extends Mock implements SurveysPresenter {
  final isLoadingController = StreamController<bool>();
  final isSessionExpiredController = StreamController<bool>();
  final surveysController = StreamController<List<SurveyViewModel>>();
  final navigateToController = StreamController<String?>();

  SurveysPresenterSpy() {
    when(() => loadData()).thenAnswer((_) async => _);
    when(() => isLoadingStream).thenAnswer((_) => isLoadingController.stream);
    when(() => surveysStream).thenAnswer((_) => surveysController.stream);
    when(() => navigateToStream).thenAnswer((_) => navigateToController.stream);
    when(() => isSessionExpiredStream).thenAnswer((_) => isSessionExpiredController.stream);
  }

  void emitSurveys(List<SurveyViewModel> data) => surveysController.add(data);
  void emitSurveysError(String error) => surveysController.addError(error);
  void emitLoading([bool show = true]) => isLoadingController.add(show);
  void emitSessionExpired([bool expired = true]) => isSessionExpiredController.add(expired);
  void emitNavigateTo(String route) => navigateToController.add(route);

  void dispose() {
    isLoadingController.close();
    surveysController.close();
    navigateToController.close();
    isSessionExpiredController.close();
  }

}
