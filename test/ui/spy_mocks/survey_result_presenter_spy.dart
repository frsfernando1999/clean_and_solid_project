import 'dart:async';

import 'package:mocktail/mocktail.dart';

import 'package:clean_flutter_app_updated/ui/pages/pages.dart';

class SurveyResultPresenterSpy extends Mock implements SurveyResultPresenter {
  final isLoadingController = StreamController<bool>();
  final isSessionExpiredController = StreamController<bool>();
  final surveyResultController = StreamController<SurveyResultViewModel?>();

  SurveyResultPresenterSpy() {
    when(() => loadData()).thenAnswer((_) async => _);
    when(() => save(answer: any(named: 'answer'))).thenAnswer((_) async => _);
    when(() => isLoadingStream).thenAnswer((_) => isLoadingController.stream);
    when(() => surveyResultStream).thenAnswer((_) => surveyResultController.stream);
    when(() => isSessionExpiredStream).thenAnswer((_) => isSessionExpiredController.stream);
  }

  void emitSurveyResult(SurveyResultViewModel? data) => surveyResultController.add(data);
  void emitSurveyResultError(String error) => surveyResultController.addError(error);
  void emitLoading([bool show = true]) => isLoadingController.add(show);
  void emitSessionExpired([bool expired = true]) => isSessionExpiredController.add(expired);

  void dispose() {
    isLoadingController.close();
    surveyResultController.close();
    isSessionExpiredController.close();
  }

}
