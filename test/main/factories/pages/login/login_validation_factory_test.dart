import 'package:test/test.dart';

import 'package:clean_flutter_app_updated/validation/validators/validators.dart';
import 'package:clean_flutter_app_updated/main/factories/factories.dart';

void main() {
  test('Should return the correct validations', () {
    //act
    final validation = makeLoginValidation();
    //assert
    expect(validation, [
      const RequiredFieldValidation('email'),
      const EmailValidation('email'),
      const RequiredFieldValidation('password'),
      const MinLengthValidation(field: 'password', size: 3),
    ]);
  });
}
