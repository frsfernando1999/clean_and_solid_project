import 'package:mocktail/mocktail.dart';
import 'package:test/test.dart';

import 'package:clean_flutter_app_updated/main/composites/composites.dart';
import 'package:clean_flutter_app_updated/domain/helpers/helpers.dart';
import 'package:clean_flutter_app_updated/domain/entities/entities.dart';

import '../../mocks/values_mocks.dart';
import '../../data/spy_mocks/spy_mocks.dart';

void main() {
  late RemoteLoadSurveysWithLocalFallback sut;
  late RemoteLoadSurveysSpy remote;
  late LocalLoadSurveysSpy local;
  late List<SurveyEntity> remoteSurveys;
  late List<SurveyEntity> localSurveys;

  setUp(() {
    remoteSurveys = EntityFactory.makeSurveyList();
    localSurveys = EntityFactory.makeSurveyList();
    remote = RemoteLoadSurveysSpy();
    local = LocalLoadSurveysSpy();
    sut = RemoteLoadSurveysWithLocalFallback(
        remote: remote,
        local: local
    );
    remote.mockRemoteLoad(remoteSurveys);
    local.mockLocalLoad(localSurveys);
  });

  test('Should call remote load', () async {
    await sut.load();

    verify(() => remote.load()).called(1);
  });

  test('Should call local save with remote data', () async {
    await sut.load();

    verify(() => local.save(remoteSurveys)).called(1);
  });

  test('Should return remote surveys', () async {
    final surveys = await sut.load();

    expect(surveys, remoteSurveys);
  });

  test('Should rethrow if remote load throws AccessDeniedError', () async {
    remote.mockRemoteError(DomainError.accessDenied);

    final future = sut.load();

    expect(future, throwsA(DomainError.accessDenied));
  });

  test('Should call local fetch on remote error', () async {
    remote.mockRemoteError(DomainError.unexpected);

    await sut.load();

    verify(() => local.validate()).called(1);
    verify(() => local.load()).called(1);
  });

  test('Should return local surveys', () async {
    remote.mockRemoteError(DomainError.unexpected);

    final surveys = await sut.load();

    expect(surveys, localSurveys);
  });

  test('Should throw unexpectedError if remote and local throws', () async {
    remote.mockRemoteError(DomainError.unexpected);
    local.mockLocalError();

    final future = sut.load();

    expect(future, throwsA(DomainError.unexpected));
  });
}
