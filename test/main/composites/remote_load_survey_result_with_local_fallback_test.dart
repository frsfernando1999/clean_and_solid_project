import 'package:faker/faker.dart';
import 'package:mocktail/mocktail.dart';
import 'package:test/test.dart';

import 'package:clean_flutter_app_updated/main/composites/composites.dart';

import 'package:clean_flutter_app_updated/domain/helpers/helpers.dart';
import 'package:clean_flutter_app_updated/domain/entities/entities.dart';

import '../../mocks/values_mocks.dart';
import '../../data/spy_mocks/spy_mocks.dart';

void main() {
  late String surveyId;
  late SurveyResultEntity localResult;
  late SurveyResultEntity remoteResult;
  late LocalLoadSurveyResultSpy local;
  late RemoteLoadSurveyResultSpy remote;
  late RemoteLoadSurveyResultWithLocalFallback sut;

  setUp(() {
    surveyId = faker.guid.guid();
    localResult = EntityFactory.makeSurveyResult();
    remoteResult = EntityFactory.makeSurveyResult();
    local = LocalLoadSurveyResultSpy();
    remote = RemoteLoadSurveyResultSpy();
    sut = RemoteLoadSurveyResultWithLocalFallback(
      remote: remote,
      local: local,
    );
    local.mockLocalLoad(localResult);
    remote.mockRemoteLoad(remoteResult);
  });

  setUpAll(() {
    registerFallbackValue(EntityFactory.makeSurveyResult());
  });

  test('Should call remote LoadBySurvey', () async {
    await sut.loadBySurvey(surveyId: surveyId);

    verify(() => remote.loadBySurvey(surveyId: surveyId)).called(1);
  });

  test('Should call Local save with remote data', () async {
    await sut.loadBySurvey(surveyId: surveyId);

    verify(() => local.save(remoteResult)).called(1);
  });

  test('Should return remote data SurveyResultEntity on loadBySurvey',
      () async {
    final response = await sut.loadBySurvey(surveyId: surveyId);

    expect(response, remoteResult);
  });

  test('Should rethrow if remote loadBySurvey throws accessDenied error',
      () async {
    remote.mockRemoteError(DomainError.accessDenied);

    final future = sut.loadBySurvey(surveyId: surveyId);

    expect(future, throwsA(DomainError.accessDenied));
  });

  test('Should call loadBySurvey on remote error', () async {
    remote.mockRemoteError(DomainError.unexpected);

    await sut.loadBySurvey(surveyId: surveyId);

    verify(() => local.validate(surveyId)).called(1);
    verify(() => local.loadBySurvey(surveyId: surveyId)).called(1);
  });

  test('Should return local data SurveyResultEntity on loadBySurvey', () async {
    remote.mockRemoteError(DomainError.unexpected);

    final response = await sut.loadBySurvey(surveyId: surveyId);

    expect(response, localResult);
  });

  test('Should throw unexpected error if local load fails', () async {
    remote.mockRemoteError(DomainError.unexpected);
    local.mockLocalError();

    final future = sut.loadBySurvey(surveyId: surveyId);

    expect(future, throwsA(DomainError.unexpected));
  });
}
