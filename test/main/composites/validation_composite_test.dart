import 'package:test/test.dart';

import 'package:clean_flutter_app_updated/presentation/protocols/protocols.dart';

import 'package:clean_flutter_app_updated/main/composites/composites.dart';

import '../../validation/spy_mocks/spy_mocks.dart';

void main() {
  late ValidationComposite sut;
  late FieldValidationSpy validation1;
  late FieldValidationSpy validation2;
  late FieldValidationSpy validation3;

  setUp(() {
    validation1 = FieldValidationSpy();
    validation1.mockFieldName('other_field');
    validation2 = FieldValidationSpy();
    validation3 = FieldValidationSpy();

    sut = ValidationComposite([validation1, validation2, validation3]);
  });

  test('Should return null if all validations returns null or empty', () {
    //act
    final error =
        sut.validate(field: 'any_field', input: {'any_field': 'any_value'});

    //assert
    expect(error, null);
  });

  test('Should return the first error', () {
    //arrange
    validation1.mockValidationError(ValidationError.requiredField);
    validation2.mockValidationError(ValidationError.requiredField);
    validation3.mockValidationError(ValidationError.invalidField);

    //act
    final error =
        sut.validate(field: 'any_field', input: {'any_field': 'any_value'});

    //assert
    expect(error, ValidationError.requiredField);
  });
}
