import 'package:flutter_test/flutter_test.dart';

import 'package:faker/faker.dart';
import 'package:mocktail/mocktail.dart';

import 'package:clean_flutter_app_updated/domain/usecases/usecases.dart';
import 'package:clean_flutter_app_updated/domain/helpers/domain_error.dart';
import 'package:clean_flutter_app_updated/domain/entities/entities.dart';

import 'package:clean_flutter_app_updated/presentation/protocols/validation.dart';
import 'package:clean_flutter_app_updated/presentation/presenters/presenters.dart';

import 'package:clean_flutter_app_updated/ui/helpers/errors/errors.dart';

import '../../domain/spy_mocks/spy_mocks.dart';
import '../../mocks/values_mocks.dart';
import '../spy_mocks/spy_mocks.dart';

void main() {
  //prepare setup
  late GetxLoginPresenter sut;
  late AuthenticationSpy authentication;
  late ValidationSpy validation;
  late SaveCurrentAccountSpy saveCurrentAccount;
  late String email;
  late String password;
  late AccountEntity account;

  setUp(() {
    email = faker.internet.email();
    password = faker.internet.password();
    account = EntityFactory.makeAccount();
    validation = ValidationSpy();
    authentication = AuthenticationSpy();
    saveCurrentAccount = SaveCurrentAccountSpy();
    authentication.mockAuthentication(account);
    sut = GetxLoginPresenter(
      validation: validation,
      authentication: authentication,
      saveCurrentAccount: saveCurrentAccount,
    );
  });

  setUpAll(() {
    registerFallbackValue(
      ParametersFactory.makeAuthentication(),
    );
    registerFallbackValue(
        EntityFactory.makeAccount(),
    );
  });

  test('Should call Validation with correct email', () {
    //arrange
    final formData = {
      'email': email,
      'password': null,
    };

    //act
    sut.validateEmail(email);
    //assert
    verify(() => validation.validate(field: 'email', input: formData)).called(1);
  });

  test('Should invalidField error if email is invalid', () {
    //arrange
    validation.mockValidationError(value: ValidationError.invalidField);
    //assert (Em casos especiais envolvendo Streams, o assert deve ser verificado logo após o arrange)
    sut.emailErrorStream
        .listen(expectAsync1((error) => expect(error, UIError.invalidField)));
    sut.isFormValidStream
        .listen(expectAsync1((isValid) => expect(isValid, false)));
    //act
    sut.validateEmail(email);
    sut.validateEmail(email);
  });

  test('Should requiredField error if email is empty', () {
    //arrange
    validation.mockValidationError(value: ValidationError.requiredField);
    //assert (Em casos especiais envolvendo Streams, o assert deve ser verificado logo após o arrange)
    sut.emailErrorStream
        .listen(expectAsync1((error) => expect(error, UIError.requiredField)));
    sut.isFormValidStream
        .listen(expectAsync1((isValid) => expect(isValid, false)));
    //act
    sut.validateEmail(email);
    sut.validateEmail(email);
  });

  test('Should emit null if validation succeeds', () {
    //arrange
    //nothing to do here

    //assert (Em casos especiais envolvendo Streams, o assert deve ser verificado logo após o arrange)
    sut.emailErrorStream.listen(expectAsync1((error) => expect(error, null)));
    sut.isFormValidStream
        .listen(expectAsync1((isValid) => expect(isValid, false)));
    //act
    sut.validateEmail(email);
    sut.validateEmail(email);
  });

  test('Should call Validation with correct password', () {
    //arrange
    final formData = {
      'email': null,
      'password': password,
    };
    //act
    sut.validatePassword(password);
    //assert
    verify(() => validation.validate(field: 'password', input: formData)).called(1);
  });

  test('Should requiredField error if password is empty', () {
    //arrange
    validation.mockValidationError(value: ValidationError.requiredField);
    //assert (Em casos especiais envolvendo Streams, o assert deve ser verificado logo após o arrange)
    sut.passwordErrorStream
        .listen(expectAsync1((error) => expect(error, UIError.requiredField)));
    sut.isFormValidStream
        .listen(expectAsync1((isValid) => expect(isValid, false)));
    //act
    sut.validatePassword(password);
    sut.validatePassword(password);
  });

  test('Should emit null if validation succeeds', () {
    //arrange
    //nothing to do here
    //assert (Em casos especiais envolvendo Streams, o assert deve ser verificado logo após o arrange)
    sut.passwordErrorStream
        .listen(expectAsync1((error) => expect(error, null)));
    sut.isFormValidStream
        .listen(expectAsync1((isValid) => expect(isValid, false)));
    //act
    sut.validatePassword(password);
    sut.validatePassword(password);
  });

  test('Should disable form button if any field is invalid', () {
    validation.mockValidationError(field: 'email', value: ValidationError.invalidField);
    //arrange
    //nothing to do here
    //assert (Em casos especiais envolvendo Streams, o assert deve ser verificado logo após o arrange)
    sut.isFormValidStream
        .listen(expectAsync1((isValid) => expect(isValid, false)));
    //act
    sut.validateEmail(email);
    sut.validatePassword(password);
  });

  test('Should enable form button if all fields are valid', () async {
    //arrange
    //nothing to do here

    //assert (Em casos especiais envolvendo Streams, o assert deve ser verificado logo após o arrange)
    expectLater(sut.isFormValidStream, emitsInOrder([false, true]));

    //act
    sut.validateEmail(email);
    await Future.delayed(Duration.zero);
    sut.validatePassword(password);
  });

  test('Should call Authentication with correct values', () async {
    //arrange
    sut.validateEmail(email);
    sut.validatePassword(password);
    //act
    await sut.auth();
    //assert
    verify(() => authentication
            .auth(AuthenticationParameters(email: email, secret: password)))
        .called(1);
  });

  test('Should call SaveCurrentAccount with correct value', () async {
    //arrange
    sut.validateEmail(email);
    sut.validatePassword(password);
    //act
    await sut.auth();
    //assert
    verify(() => saveCurrentAccount.save(account)).called(1);
  });

  test('Should emit UnexpectedError if SaveCurrentAccount fails', () async {
    saveCurrentAccount.mockSaveCurrentAccountError();
    //arrange
    sut.validateEmail(email);
    sut.validatePassword(password);
    //assert
    expectLater(sut.mainErrorStream, emitsInOrder([null, UIError.unexpected]));
    expectLater(sut.isLoadingStream, emitsInOrder([true, false]));
    //act
    await sut.auth();
  });

  test('Should emit correct events on Authentication success', () async {
    //arrange
    sut.validateEmail(email);
    sut.validatePassword(password);
    //assert
    expectLater(sut.mainErrorStream, emits(null));
    expectLater(sut.isLoadingStream, emits(true));

    //act
    await sut.auth();
  });

  test('Should change page on success', () async {
    //arrange
    sut.validateEmail(email);
    sut.validatePassword(password);
    //assert
    sut.navigateToStream
        .listen(expectAsync1((page) => expect(page, '/surveys')));

    //act
    await sut.auth();
  });

  test('Should emit correct events on InvalidCredentialsError', () async {
    authentication.mockAuthenticationError(DomainError.invalidCredentials);
    //arrange
    sut.validateEmail(email);
    sut.validatePassword(password);
    //assert
    expectLater(sut.isLoadingStream, emitsInOrder([true, false]));
    expectLater(sut.mainErrorStream, emitsInOrder([null, UIError.invalidCredentials]));

    //act
    await sut.auth();
  });

  test('Should emit correct events on UnexpectedError', () async {
    authentication.mockAuthenticationError(DomainError.unexpected);
    //arrange
    sut.validateEmail(email);
    sut.validatePassword(password);
    //assert
    expectLater(sut.isLoadingStream, emitsInOrder([true, false]));
    expectLater(sut.mainErrorStream, emitsInOrder([null, UIError.unexpected]));
    //act
    await sut.auth();
  });

  test('Should go to signUp on link click', () async {
    sut.navigateToStream
        .listen(expectAsync1((page) => expect(page, '/signup')));
    sut.goToSignUp();

    //act
  });
}
