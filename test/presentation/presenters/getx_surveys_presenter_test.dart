import 'package:test/test.dart';
import 'package:mocktail/mocktail.dart';

import 'package:clean_flutter_app_updated/domain/entities/entities.dart';
import 'package:clean_flutter_app_updated/domain/helpers/helpers.dart';

import 'package:clean_flutter_app_updated/presentation/presenters/presenters.dart';

import 'package:clean_flutter_app_updated/ui/helpers/errors/errors.dart';
import 'package:clean_flutter_app_updated/ui/pages/surveys/surveys.dart';

import '../../data/spy_mocks/spy_mocks.dart';
import '../../mocks/values_mocks.dart';

void main() {
  late GetXSurveysPresenter sut;
  late LoadSurveysSpy loadSurveys;
  late List<SurveyEntity> surveys;

  setUp(() {
    surveys = EntityFactory.makeSurveyList();
    loadSurveys = LoadSurveysSpy();
    sut = GetXSurveysPresenter(loadSurveys: loadSurveys);
    loadSurveys.mockLoad(surveys);
  });

  test('Should call LoadSurveys on loadData', () async {
    //act
    await sut.loadData();
    //assert
    verify(() => loadSurveys.load()).called(1);
  });

  test('Should emit correct events on success', () async {
    expectLater(sut.isLoadingStream, emitsInOrder([true, false]));
    sut.surveysStream.listen(expectAsync1((surveys) => expect(surveys, [
          SurveyViewModel(
              id: surveys[0].id,
              question: surveys[0].question,
              date: '02 Feb 2020',
              didAnswer: surveys[0].didAnswer),
          SurveyViewModel(
              id: surveys[1].id,
              question: surveys[1].question,
              date: '20 Dec 2018',
              didAnswer: surveys[1].didAnswer),
        ])));

    await sut.loadData();
  });

  test('Should emit correct events on failure', () async {
    loadSurveys.mockLoadError(DomainError.unexpected);
    expectLater(sut.isLoadingStream, emitsInOrder([true, false]));
    sut.surveysStream.listen(null,
        onError: expectAsync1(
            (error) => expect(error, UIError.unexpected.description)));

    await sut.loadData();
  });

  test('Should emit correct on accessDenied', () async {
    loadSurveys.mockLoadError(DomainError.accessDenied);
    expectLater(sut.isLoadingStream, emitsInOrder([true, false]));
    expectLater(sut.isSessionExpiredStream, emits(true));

    await sut.loadData();
  });

  test('Should go to SurveyResultPage on survey click', () async {
    expectLater(
        sut.navigateToStream,
        emitsInOrder([
          '/survey_result/any_route',
          '/survey_result/any_route',
        ]));

    sut.goToSurveyResult('any_route');
    sut.goToSurveyResult('any_route');
  });
}
