import 'package:flutter_test/flutter_test.dart';

import 'package:faker/faker.dart';
import 'package:mocktail/mocktail.dart';

import 'package:clean_flutter_app_updated/domain/helpers/helpers.dart';
import 'package:clean_flutter_app_updated/domain/usecases/usecases.dart';
import 'package:clean_flutter_app_updated/domain/entities/entities.dart';
import 'package:clean_flutter_app_updated/presentation/protocols/validation.dart';
import 'package:clean_flutter_app_updated/presentation/presenters/presenters.dart';

import 'package:clean_flutter_app_updated/ui/helpers/errors/errors.dart';

import '../../domain/spy_mocks/spy_mocks.dart';
import '../../mocks/values_mocks.dart';
import '../spy_mocks/spy_mocks.dart';


void main() {
  //prepare setup
  late GetxSignupPresenter sut;
  late ValidationSpy validation;
  late AddAccountSpy addAccount;
  late SaveCurrentAccountSpy saveCurrentAccount;
  late String email;
  late String name;
  late String password;
  late String passwordConfirmation;
  late AccountEntity account;


  setUp(() {
    email = faker.internet.email();
    name = faker.person.name();
    password = faker.internet.password();
    passwordConfirmation = faker.internet.password();
    account = EntityFactory.makeAccount();
    addAccount = AddAccountSpy();
    validation = ValidationSpy();
    saveCurrentAccount = SaveCurrentAccountSpy();
    addAccount.mockAddAccount(account);
    sut = GetxSignupPresenter(
      validation: validation,
      addAccount: addAccount,
      saveCurrentAccount: saveCurrentAccount,
    );
  });

  setUpAll(() {
    registerFallbackValue(EntityFactory.makeAccount());
    registerFallbackValue(ParametersFactory.makeAddAccount());
  });

  test('Should call Validation with correct email', () {
    //arrange
    final formData = {
      'email': email,
      'name': null,
      'password': null,
      'passwordConfirmation': null,
    };

    //act
    sut.validateEmail(email);
    //assert
    verify(() => validation.validate(field: 'email', input: formData)).called(1);
  });

  test('Should emit invalidField error if email is invalid', () {
    //arrange
    validation.mockValidationError(value: ValidationError.invalidField);
    //assert (Em casos especiais envolvendo Streams, o assert deve ser verificado logo após o arrange)
    sut.emailErrorStream
        .listen(expectAsync1((error) => expect(error, UIError.invalidField)));
    sut.isFormValidStream
        .listen(expectAsync1((isValid) => expect(isValid, false)));
    //act
    sut.validateEmail(email);
    sut.validateEmail(email);
  });

  test('Should emit requiredField error if email is empty', () {
    //arrange
    validation.mockValidationError(value: ValidationError.requiredField);
    //assert (Em casos especiais envolvendo Streams, o assert deve ser verificado logo após o arrange)
    sut.emailErrorStream
        .listen(expectAsync1((error) => expect(error, UIError.requiredField)));
    sut.isFormValidStream
        .listen(expectAsync1((isValid) => expect(isValid, false)));
    //act
    sut.validateEmail(email);
    sut.validateEmail(email);
  });

  test('Should emit null if email validation succeeds', () {
    //assert (Em casos especiais envolvendo Streams, o assert deve ser verificado logo após o arrange)
    sut.emailErrorStream.listen(expectAsync1((error) => expect(error, null)));
    sut.isFormValidStream
        .listen(expectAsync1((isValid) => expect(isValid, false)));
    //act
    sut.validateEmail(email);
    sut.validateEmail(email);
  });

  test('Should call Validation with correct name', () {
    //arrange
    final formData = {
      'email': null,
      'name': name,
      'password': null,
      'passwordConfirmation': null,
    };

    //act
    sut.validateName(name);
    //assert
    verify(() => validation.validate(field: 'name', input: formData)).called(1);
  });

  test('Should emit invalidField error if name is invalid', () {
    //arrange
    validation.mockValidationError(value: ValidationError.invalidField);
    //assert (Em casos especiais envolvendo Streams, o assert deve ser verificado logo após o arrange)
    sut.nameErrorStream
        .listen(expectAsync1((error) => expect(error, UIError.invalidField)));
    sut.isFormValidStream
        .listen(expectAsync1((isValid) => expect(isValid, false)));
    //act
    sut.validateName(name);
    sut.validateName(name);
  });

  test('Should emit requiredField error if name is empty', () {
    //arrange
    validation.mockValidationError(value: ValidationError.requiredField);
    //assert (Em casos especiais envolvendo Streams, o assert deve ser verificado logo após o arrange)
    sut.nameErrorStream
        .listen(expectAsync1((error) => expect(error, UIError.requiredField)));
    sut.isFormValidStream
        .listen(expectAsync1((isValid) => expect(isValid, false)));
    //act
    sut.validateName(name);
    sut.validateName(name);
  });

  test('Should emit null if name validation succeeds', () {
    //arrange
    //nothing to do here

    //assert (Em casos especiais envolvendo Streams, o assert deve ser verificado logo após o arrange)
    sut.nameErrorStream.listen(expectAsync1((error) => expect(error, null)));
    sut.isFormValidStream
        .listen(expectAsync1((isValid) => expect(isValid, false)));
    //act
    sut.validateName(name);
    sut.validateName(name);
  });

  test('Should call Validation with correct password', () {
    //arrange
    final formData = {
      'email': null,
      'name': null,
      'password': password,
      'passwordConfirmation': null,
    };

    //act
    sut.validatePassword(password);
    //assert
    verify(() => validation.validate(field: 'password', input: formData)).called(1);
  });

  test('Should emit invalidField error if password is invalid', () {
    //arrange
    validation.mockValidationError(value: ValidationError.invalidField);
    //assert (Em casos especiais envolvendo Streams, o assert deve ser verificado logo após o arrange)
    sut.passwordErrorStream
        .listen(expectAsync1((error) => expect(error, UIError.invalidField)));
    sut.isFormValidStream
        .listen(expectAsync1((isValid) => expect(isValid, false)));
    //act
    sut.validatePassword(password);
    sut.validatePassword(password);
  });

  test('Should emit requiredField error if password is empty', () {
    //arrange
    validation.mockValidationError(value: ValidationError.requiredField);
    //assert (Em casos especiais envolvendo Streams, o assert deve ser verificado logo após o arrange)
    sut.passwordErrorStream
        .listen(expectAsync1((error) => expect(error, UIError.requiredField)));
    sut.isFormValidStream
        .listen(expectAsync1((isValid) => expect(isValid, false)));
    //act
    sut.validatePassword(password);
    sut.validatePassword(password);
  });

  test('Should emit null if password validation succeeds', () {
    //arrange
    //nothing to do here

    //assert (Em casos especiais envolvendo Streams, o assert deve ser verificado logo após o arrange)
    sut.passwordErrorStream
        .listen(expectAsync1((error) => expect(error, null)));
    sut.isFormValidStream
        .listen(expectAsync1((isValid) => expect(isValid, false)));
    //act
    sut.validatePassword(password);
    sut.validatePassword(password);
  });
  test('Should call Validation with correct passwordConfirmation', () {
    //arrange
    final formData = {
      'email': null,
      'name': null,
      'password': null,
      'passwordConfirmation': passwordConfirmation,
    };

    //act
    sut.validatePasswordConfirmation(passwordConfirmation);
    //assert
    verify(() => validation.validate(field: 'passwordConfirmation', input: formData))
        .called(1);
  });

  test('Should emit invalidField error if passwordConfirmation is invalid', () {
    //arrange
    validation.mockValidationError(value: ValidationError.invalidField);
    //assert (Em casos especiais envolvendo Streams, o assert deve ser verificado logo após o arrange)
    sut.passwordConfirmationErrorStream
        .listen(expectAsync1((error) => expect(error, UIError.invalidField)));
    sut.isFormValidStream
        .listen(expectAsync1((isValid) => expect(isValid, false)));
    //act
    sut.validatePasswordConfirmation(passwordConfirmation);
    sut.validatePasswordConfirmation(passwordConfirmation);
  });

  test('Should emit requiredField error if passwordConfirmation is empty', () {
    //arrange
    validation.mockValidationError(value: ValidationError.requiredField);
    //assert (Em casos especiais envolvendo Streams, o assert deve ser verificado logo após o arrange)
    sut.passwordConfirmationErrorStream
        .listen(expectAsync1((error) => expect(error, UIError.requiredField)));
    sut.isFormValidStream
        .listen(expectAsync1((isValid) => expect(isValid, false)));
    //act
    sut.validatePasswordConfirmation(passwordConfirmation);
    sut.validatePasswordConfirmation(passwordConfirmation);
  });

  test('Should emit null if passwordConfirmation validation succeeds', () {
    //arrange
    //nothing to do here

    //assert (Em casos especiais envolvendo Streams, o assert deve ser verificado logo após o arrange)
    sut.passwordConfirmationErrorStream
        .listen(expectAsync1((error) => expect(error, null)));
    sut.isFormValidStream
        .listen(expectAsync1((isValid) => expect(isValid, false)));
    //act
    sut.validatePasswordConfirmation(passwordConfirmation);
    sut.validatePasswordConfirmation(passwordConfirmation);
  });

  test('Should enable form button if all fields are valid', () async {
    //arrange
    //nothing to do here

    //assert (Em casos especiais envolvendo Streams, o assert deve ser verificado logo após o arrange)
    expectLater(sut.isFormValidStream, emitsInOrder([false, true]));

    //act
    sut.validateEmail(email);
    await Future.delayed(Duration.zero);
    sut.validateName(name);
    await Future.delayed(Duration.zero);
    sut.validatePassword(password);
    await Future.delayed(Duration.zero);
    sut.validatePasswordConfirmation(passwordConfirmation);
    await Future.delayed(Duration.zero);
  });

  test('Should call AddAccount with correct values', () async {
    //arrange
    sut.validateEmail(email);
    sut.validateName(name);
    sut.validatePassword(password);
    sut.validatePasswordConfirmation(passwordConfirmation);
    //act
    await sut.signup();
    //assert
    verify(() => addAccount.add(AddAccountParameters(
      name: name,
      email: email,
      password: password,
      passwordConfirmation: passwordConfirmation,
    ))).called(1);
  });

  test('Should call SaveCurrentAccount with correct value', () async {
    //arrange
    sut.validateEmail(email);
    sut.validateName(name);
    sut.validatePassword(password);
    sut.validatePasswordConfirmation(passwordConfirmation);
    //act
    await sut.signup();
    //assert
    verify(() => saveCurrentAccount.save(account)).called(1);
  });

  test('Should emit UnexpectedError if SaveCurrentAccount fails', () async {
    saveCurrentAccount.mockSaveCurrentAccountError();
    //arrange
    sut.validateEmail(email);
    sut.validateName(name);
    sut.validatePassword(password);
    sut.validatePasswordConfirmation(passwordConfirmation);
    //assert
    expectLater(sut.isLoadingStream, emitsInOrder([true, false]));
    expectLater(sut.mainErrorStream, emitsInOrder([null, UIError.unexpected]));
    //act
    await sut.signup();
  });

  test('Should emit correct events on AddAccount success', () async {
    //arrange
    sut.validateEmail(email);
    sut.validateName(name);
    sut.validatePassword(password);
    sut.validatePasswordConfirmation(passwordConfirmation);
    //assert
    expectLater(sut.mainErrorStream, emits(null));
    expectLater(sut.isLoadingStream, emits(true));

    //act
    await sut.signup();
  });

  test('Should change page on success', () async {
    //arrange
    sut.validateEmail(email);
    sut.validateName(name);
    sut.validatePassword(password);
    sut.validatePasswordConfirmation(passwordConfirmation);
    //assert
    sut.navigateToStream
        .listen(expectAsync1((page) => expect(page, '/surveys')));

    //act
    await sut.signup();
  });

  test('Should emit correct events on InvalidCredentialsError', () async {
    addAccount.mockAddAccountError(DomainError.emailInUse);
    //arrange
    sut.validateEmail(email);
    sut.validateName(name);
    sut.validatePassword(password);
    sut.validatePasswordConfirmation(passwordConfirmation);
    //assert
    expectLater(sut.isLoadingStream, emitsInOrder([true, false]));
    expectLater(sut.mainErrorStream, emitsInOrder([null, UIError.emailInUse]));

    //act
    await sut.signup();
  });

  test('Should emit correct events on UnexpectedError', () async {
    addAccount.mockAddAccountError(DomainError.unexpected);
    //arrange
    sut.validateEmail(email);
    sut.validateName(name);
    sut.validatePassword(password);
    sut.validatePasswordConfirmation(passwordConfirmation);
    //assert
    expectLater(sut.isLoadingStream, emitsInOrder([true, false]));
    expectLater(sut.mainErrorStream, emitsInOrder([null, UIError.unexpected]));
    //act
    await sut.signup();
  });

  test('Should go to login on link click', () async {
    sut.navigateToStream.listen(expectAsync1((page) => expect(page, '/login')));
    sut.goToLogin();

    //act
  });
}
