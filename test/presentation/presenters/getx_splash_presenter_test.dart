import 'package:mocktail/mocktail.dart';
import 'package:test/test.dart';

import 'package:clean_flutter_app_updated/presentation/presenters/presenters.dart';

import '../../domain/spy_mocks/spy_mocks.dart';
import '../../mocks/values_mocks.dart';

void main() {
  late LoadCurrentAccountSpy loadCurrentAccount;
  late GetxSplashPresenter sut;

  setUp(() {
    loadCurrentAccount = LoadCurrentAccountSpy();
    sut = GetxSplashPresenter(loadCurrentAccount: loadCurrentAccount);
    loadCurrentAccount.mockLoad(account: EntityFactory.makeAccount());
  });

  setUpAll(() {
    registerFallbackValue(EntityFactory.makeAccount());
  });

  test('Should call LoadCurrentAccount', () async {
    //act
    await sut.checkCurrentAccount(durationInMilliseconds: 0);
    //assert
    verify(() => loadCurrentAccount.load()).called(1);
  });

  test('Should go to surveys page on success', () async {
    //assert
    sut.navigateToStream
        .listen(expectAsync1((page) => expect(page, '/surveys')));
    //act
    await sut.checkCurrentAccount(durationInMilliseconds: 0);
  });

  test('Should go to login page on error', () async {
    //arrange
    loadCurrentAccount.mockLoadError();
    //assert
    sut.navigateToStream.listen(expectAsync1((page) => expect(page, '/login')));
    //act
    await sut.checkCurrentAccount(durationInMilliseconds: 0);
  });
}
