import 'package:flutter_test/flutter_test.dart';

import 'package:faker/faker.dart';
import 'package:mocktail/mocktail.dart';

import 'package:clean_flutter_app_updated/domain/helpers/domain_error.dart';
import 'package:clean_flutter_app_updated/domain/entities/account_entity.dart';
import 'package:clean_flutter_app_updated/domain/usecases/authentication.dart';

import 'package:clean_flutter_app_updated/presentation/protocols/validation.dart';
import 'package:clean_flutter_app_updated/presentation/presenters/presenters.dart';

import 'package:clean_flutter_app_updated/ui/helpers/errors/errors.dart';

import '../../domain/mocks/mocks.dart';
import '../../domain/spy_mocks/spy_mocks.dart';
import '../spy_mocks/spy_mocks.dart';

void main() {
  //prepare setup
  late StreamLoginPresenter sut;
  late AuthenticationSpy authentication;
  late ValidationSpy validation;
  late String email;
  late String password;
  late AccountEntity account;

  setUp(() {
    account = EntityFactory.makeAccount();
    validation = ValidationSpy();
    authentication = AuthenticationSpy();
    sut = StreamLoginPresenter(
        validation: validation, authentication: authentication);
    email = faker.internet.email();
    password = faker.internet.password();
    authentication.mockAuthentication(account);
  });

  setUpAll(() {
    registerFallbackValue(
      ParametersFactory.makeAuthentication(),
    );
    registerFallbackValue(
      EntityFactory.makeAccount(),
    );
  });


  test('Should call Validation with correct email', () {
    //arrange
    final formData = {
      'email': email,
      'password': null,
    };

    //act
    sut.validateEmail(email);
    //assert
    verify(() => validation.validate(field: 'email', input: formData)).called(1);
  });

  test('Should emit email error if validation fails', () {
    //arrange
    validation.mockValidationError(value: ValidationError.invalidField);
    //assert (Em casos especiais envolvendo Streams, o assert deve ser verificado logo após o arrange)
    sut.emailErrorStream
        .listen(expectAsync1((error) => expect(error, UIError.invalidField)));
    sut.isFormValidStream
        .listen(expectAsync1((isValid) => expect(isValid, false)));
    //act
    sut.validateEmail(email);
    sut.validateEmail(email);
  });

  test('Should emit null if validation succeeds', () {
    //arrange
    //nothing to do here

    //assert (Em casos especiais envolvendo Streams, o assert deve ser verificado logo após o arrange)
    sut.emailErrorStream.listen(expectAsync1((error) => expect(error, null)));
    sut.isFormValidStream
        .listen(expectAsync1((isValid) => expect(isValid, false)));
    //act
    sut.validateEmail(email);
    sut.validateEmail(email);
  });

  test('Should call Validation with correct password', () {
    //arrange
    final formData = {'email': null, 'password': password};
    //act
    sut.validatePassword(password);
    //assert
    verify(() => validation.validate(field: 'password', input: formData)).called(1);
  });

  test('Should emit no error when no value is passed on password', () {
    //assert (Em casos especiais envolvendo Streams, o assert deve ser verificado logo após o arrange)
    sut.passwordErrorStream
        .listen(expectAsync1((error) => expect(error, null)));
    sut.isFormValidStream
        .listen(expectAsync1((isValid) => expect(isValid, false)));
    //act
    sut.validatePassword(password);
    sut.validatePassword(password);
  });

  test('Should emit password error if validation fails', () async {
    //assert (Em casos especiais envolvendo Streams, o assert deve ser verificado logo após o arrange)
    sut.emailErrorStream.listen(expectAsync1((error) => expect(error, null)));
    sut.passwordErrorStream
        .listen(expectAsync1((error) => expect(error, null)));
    expectLater(sut.isFormValidStream, emitsInOrder([false, true]));

    //act
    sut.validateEmail(email);
    await Future.delayed(Duration.zero);
    sut.validatePassword(password);
  });

  test('Should call Authentication with correct values', () async {
    //arrange
    sut.validateEmail(email);
    sut.validatePassword(password);
    //act
    await sut.auth();
    //assert
    verify(() => authentication
            .auth(AuthenticationParameters(email: email, secret: password)))
        .called(1);
  });

  test('Should emit correct events on Authentication success', () async {
    //arrange
    sut.validateEmail(email);
    sut.validatePassword(password);
    //assert
    expectLater(sut.isLoadingStream, emitsInOrder([true, false]));

    //act
    await sut.auth();
  });

  test('Should emit correct events on InvalidCredentialsError', () async {
    authentication.mockAuthenticationError(DomainError.invalidCredentials);
    //arrange
    sut.validateEmail(email);
    sut.validatePassword(password);
    //assert
    expectLater(sut.isLoadingStream, emits(false));
    sut.mainErrorStream.listen(
        expectAsync1((error) => expect(error, UIError.invalidCredentials)));

    //act
    await sut.auth();
  });

  test('Should emit correct events on UnexpectedError', () async {
    authentication.mockAuthenticationError(DomainError.unexpected);
    //arrange
    sut.validateEmail(email);
    sut.validatePassword(password);
    //assert
    expectLater(sut.isLoadingStream, emits(false));
    sut.mainErrorStream
        .listen(expectAsync1((error) => expect(error, UIError.unexpected)));
    //act
    await sut.auth();
  });

  test('Should not emit after dispose', () async {
    expectLater(sut.emailErrorStream, neverEmits(null));
    sut.dispose();
  });
}
