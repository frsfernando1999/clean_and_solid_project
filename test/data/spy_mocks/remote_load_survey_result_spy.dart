import 'package:mocktail/mocktail.dart';

import 'package:clean_flutter_app_updated/data/usecases/load_survey_result/load_survey_result.dart';
import 'package:clean_flutter_app_updated/domain/entities/entities.dart';
import 'package:clean_flutter_app_updated/domain/helpers/helpers.dart';

class RemoteLoadSurveyResultSpy extends Mock implements RemoteLoadSurveyResult {
  When mockLoadBySurveyCall() => when(() => loadBySurvey(surveyId: any(named: 'surveyId')));
  void mockRemoteLoad(SurveyResultEntity entity) => mockLoadBySurveyCall().thenAnswer((_) async => entity);
  void mockRemoteError(DomainError error) => mockLoadBySurveyCall().thenThrow(error);
}