import 'package:clean_flutter_app_updated/domain/usecases/usecases.dart';
import 'package:mocktail/mocktail.dart';

import 'package:clean_flutter_app_updated/domain/entities/entities.dart';
import 'package:clean_flutter_app_updated/domain/helpers/helpers.dart';

class LoadSurveyResultSpy extends Mock implements LoadSurveyResult {
  When mockLoadSurveyCall() => when(() => loadBySurvey(surveyId: any(named: 'surveyId')));
  void mockLoad(SurveyResultEntity entity) => mockLoadSurveyCall().thenAnswer((_) async => entity);
  void mockLoadError(DomainError error) => mockLoadSurveyCall().thenThrow(error);
}