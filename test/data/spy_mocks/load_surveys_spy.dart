import 'package:mocktail/mocktail.dart';

import 'package:clean_flutter_app_updated/domain/usecases/usecases.dart';
import 'package:clean_flutter_app_updated/domain/entities/entities.dart';
import 'package:clean_flutter_app_updated/domain/helpers/helpers.dart';

class LoadSurveysSpy extends Mock implements LoadSurveys {

  When mockLoadCall() => when(() => load());
  void mockLoad(List<SurveyEntity> surveys) => mockLoadCall().thenAnswer((_) async => surveys);
  void mockLoadError(DomainError error) => mockLoadCall().thenThrow(error);
}