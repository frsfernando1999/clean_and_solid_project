import 'package:mocktail/mocktail.dart';

import 'package:clean_flutter_app_updated/data/usecases/usecases.dart';
import 'package:clean_flutter_app_updated/domain/entities/entities.dart';
import 'package:clean_flutter_app_updated/domain/helpers/helpers.dart';

class LocalLoadSurveyResultSpy extends Mock implements LocalLoadSurveyResult {
  LocalLoadSurveyResultSpy(){
    mockValidate();
    mockSave();
  }
  When mockLoadBySurveyCall() => when(() => loadBySurvey(surveyId: any(named: 'surveyId')));
  void mockLocalLoad(SurveyResultEntity entity) => mockLoadBySurveyCall().thenAnswer((_) async => entity);
  void mockLocalError() => mockLoadBySurveyCall().thenThrow(DomainError.unexpected);

  When mockValidateCall() => when(() => validate(any()));
  void mockValidate() => mockValidateCall().thenAnswer((_) async => _);
  void mockValidateError() => mockValidateCall().thenThrow(Exception());

  When mockSaveCall() => when(() => save(any()));
  void mockSave() => mockSaveCall().thenAnswer((_) async => _);
  void mockSaveError() => mockSaveCall().thenThrow(Exception());
}
