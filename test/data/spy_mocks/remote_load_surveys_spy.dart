import 'package:mocktail/mocktail.dart';

import 'package:clean_flutter_app_updated/data/usecases/usecases.dart';
import 'package:clean_flutter_app_updated/domain/entities/entities.dart';
import 'package:clean_flutter_app_updated/domain/helpers/helpers.dart';

class RemoteLoadSurveysSpy extends Mock implements RemoteLoadSurveys {

  When mockLoadCall() => when(() => load());
  void mockRemoteLoad(List<SurveyEntity> surveys) => mockLoadCall().thenAnswer((_) async => surveys);
  void mockRemoteError(DomainError error) => mockLoadCall().thenThrow(error);
}