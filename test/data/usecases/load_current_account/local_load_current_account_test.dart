import 'package:faker/faker.dart';
import 'package:mocktail/mocktail.dart';
import 'package:test/test.dart';

import 'package:clean_flutter_app_updated/data/usecases/usecases.dart';
import 'package:clean_flutter_app_updated/domain/helpers/helpers.dart';
import 'package:clean_flutter_app_updated/domain/entities/entities.dart';

import '../../spy_mocks/spy_mocks.dart';

void main() {
  late SecureCacheStorageSpy secureCacheStorage;
  late LocalLoadCurrentAccount sut;
  late String token;

  setUp(() {
    token = faker.guid.guid();
    secureCacheStorage = SecureCacheStorageSpy();
    secureCacheStorage.mockFetch(token);
    sut = LocalLoadCurrentAccount(
        fetchSecureCacheStorage: secureCacheStorage);
  });

  test('Should call FetchSecureCacheStorage with correct value', () async {
    //arrange
    //act
    await sut.load();
    //assert
    verify(() => secureCacheStorage.fetch('token'));
  });

  test('Should return an AccountEntity', () async {
    //arrange
    //act
    final account = await sut.load();
    //assert
    expect(account, AccountEntity(token: token));
  });

  test('Should throw UnexpectedError if FetchSecureCacheStorageThrows',
      () async {
    //arrange
        secureCacheStorage.mockFetchError();
    //act
    final future = sut.load();
    //assert
    expect(future, throwsA(DomainError.unexpected));
  });

  test('Should throw UnexpectedError if FetchSecureCacheStorageThrows returns null',
      () async {
    //arrange
        secureCacheStorage.mockFetch(null);
    //act
    final future = sut.load();
    //assert
    expect(future, throwsA(DomainError.unexpected));
  });
}
