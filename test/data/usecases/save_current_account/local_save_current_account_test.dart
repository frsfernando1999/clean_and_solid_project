import 'package:faker/faker.dart';
import 'package:mocktail/mocktail.dart';
import 'package:test/test.dart';

import 'package:clean_flutter_app_updated/data/usecases/usecases.dart';
import 'package:clean_flutter_app_updated/domain/helpers/domain_error.dart';
import 'package:clean_flutter_app_updated/domain/entities/account_entity.dart';

import '../../spy_mocks/spy_mocks.dart';

void main() {
  late SecureCacheStorageSpy secureCacheStorage;
  late LocalSaveCurrentAccount sut;
  late AccountEntity account;

  setUp(() {
    secureCacheStorage = SecureCacheStorageSpy();
    sut =
        LocalSaveCurrentAccount(saveSecureCacheStorage: secureCacheStorage);
    account = AccountEntity(token: faker.guid.guid());
  });

  test('Should call save SaveSecureCacheStorage with correct values', () async {
    //arrange
    //act
    await sut.save(account);
    //assert
    verify(() => secureCacheStorage.save(key: 'token', value: account.token));
  });

  test('Should throw UnexpectedError if SaveSecureCacheStorage throws',
      () async {
    //arrange
        secureCacheStorage.mockSaveError();
    //act
    final future = sut.save(account);
    //assert
    expect(future, throwsA(DomainError.unexpected));
  });
}
