import 'package:test/test.dart';

import 'package:faker/faker.dart';
import 'package:mocktail/mocktail.dart';

import 'package:clean_flutter_app_updated/data/usecases/usecases.dart';
import 'package:clean_flutter_app_updated/data/http/http.dart';

import 'package:clean_flutter_app_updated/domain/helpers/helpers.dart';
import 'package:clean_flutter_app_updated/domain/entities/entities.dart';

import '../../../mocks/values_mocks.dart';
import '../../spy_mocks/spy_mocks.dart';

void main() {
  late RemoteLoadSurveyResult sut;
  late HttpClientSpy httpClient;
  late String url;
  late Map surveyResult;
  late String surveyId;

  setUp(() {
    surveyId = faker.guid.guid();
    url = faker.internet.httpsUrl();
    surveyResult = ApiFactory.makeSurveyResultJson();
    httpClient = HttpClientSpy();
    httpClient.mockRequest(surveyResult);
    sut = RemoteLoadSurveyResult(url: url, httpClient: httpClient);
  });

  test('Should call HttpClient with correct values', () async {
    await sut.loadBySurvey(surveyId: surveyId);
    verify(() => httpClient.request(url: url, method: 'get'));
  });

  test('Should return surveyResult on 200', () async {
    final result = await sut.loadBySurvey(surveyId: surveyId);
    expect(
        result,
        SurveyResultEntity(
            surveyId: surveyResult['surveyId'],
            question: surveyResult['question'],
            answers: [
              SurveyAnswerEntity(
                image: surveyResult['answers'][0]['image'],
                answer: surveyResult['answers'][0]['answer'],
                isCurrentAnswer: surveyResult['answers'][0]
                    ['isCurrentAccountAnswer'],
                percent: surveyResult['answers'][0]['percent'],
              ),
              SurveyAnswerEntity(
                answer: surveyResult['answers'][1]['answer'],
                isCurrentAnswer: surveyResult['answers'][1]
                    ['isCurrentAccountAnswer'],
                percent: surveyResult['answers'][1]['percent'],
              ),
            ]));
  });

  test(
      'Should throw UnexpectedError if httpClient returns 200 with invalid data',
      () async {
        httpClient.mockRequest(ApiFactory.makeInvalidJson());
    final future = sut.loadBySurvey(surveyId: surveyId);

    expect(future, throwsA(DomainError.unexpected));
  });

  test('Should throw unexpected error if http client returns 404', () async {
    httpClient.mockRequestError(HttpError.notFound);
    //act
    final future = sut.loadBySurvey(surveyId: surveyId);
    //assert
    expect(future, throwsA(DomainError.unexpected));
  });

  test('Should throw unexpected error if http client returns 500', () async {
    httpClient.mockRequestError(HttpError.serverError);
    //act
    final future = sut.loadBySurvey(surveyId: surveyId);
    //assert
    expect(future, throwsA(DomainError.unexpected));
  });

  test('Should throw accessDenied if http client returns 403', () async {
    httpClient.mockRequestError(HttpError.forbidden);
    //act
    final future = sut.loadBySurvey(surveyId: surveyId);
    //assert
    expect(future, throwsA(DomainError.accessDenied));
  });
}
