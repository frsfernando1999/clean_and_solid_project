import 'package:faker/faker.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

import 'package:clean_flutter_app_updated/domain/helpers/helpers.dart';
import 'package:clean_flutter_app_updated/domain/usecases/usecases.dart';

import 'package:clean_flutter_app_updated/data/http/http.dart';
import 'package:clean_flutter_app_updated/data/usecases/usecases.dart';

import '../../../mocks/values_mocks.dart';
import '../../spy_mocks/spy_mocks.dart';

// Usar Mock para testar classes abstratas
// Triple A (Arrange/act/assert) Design Pattern
void main() {
  late RemoteAuthentication sut;
  late HttpClientSpy httpClient;
  late String url;
  late AuthenticationParameters parameters;
  late Map apiResult;

  setUp(() {
    // sut = system under test
    httpClient = HttpClientSpy();
    url = faker.internet.httpsUrl();
    sut = RemoteAuthentication(httpClient: httpClient, url: url);
    parameters = ParametersFactory.makeAuthentication();
    apiResult = ApiFactory.makeAccountJson();
    httpClient.mockRequest(apiResult);
  });

  test('Should call HttpClient with correct values', () async {
    //act
    await sut.auth(parameters);
    //assert
    verify(() => httpClient.request(
      url: url,
      method: 'post',
      body: {
        'email': parameters.email,
        'password': parameters.secret,
      },
    ));
  });

  test('Should throw unexpected error if http client returns 400', () async {
    httpClient.mockRequestError(HttpError.badRequest);
    //act
    final future = sut.auth(parameters);
    //assert
    expect(future, throwsA(DomainError.unexpected));
  });

  test('Should throw unexpected error if http client returns 404', () async {
    httpClient.mockRequestError(HttpError.notFound);
    //act
    final future = sut.auth(parameters);
    //assert
    expect(future, throwsA(DomainError.unexpected));
  });

  test('Should throw unexpected error if http client returns 500', () async {
    httpClient.mockRequestError(HttpError.serverError);
    //act
    final future = sut.auth(parameters);
    //assert
    expect(future, throwsA(DomainError.unexpected));
  });

  test('Should throw InvalidCredentialsError if http client returns 401',
      () async {
        httpClient.mockRequestError(HttpError.unauthorized);
    //act
    final future = sut.auth(parameters);
    //assert
    expect(future, throwsA(DomainError.invalidCredentials));
  });

  test('Should return an Account if http client returns 200', () async {
    //act
    final account = await sut.auth(parameters);
    //assert
    expect(account.token, apiResult['accessToken']);
  });

  test(
      'Should throw unexpected error if http client returns 200 with invalid data',
      () async {
        httpClient.mockRequest({'invalid_key': 'invalid_value'});
    //act
    final future = sut.auth(parameters);
    //assert
    expect(future, throwsA(DomainError.unexpected));
  });
}
