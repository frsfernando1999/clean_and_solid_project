import 'package:faker/faker.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

import 'package:clean_flutter_app_updated/domain/usecases/usecases.dart';
import 'package:clean_flutter_app_updated/domain/helpers/helpers.dart';

import 'package:clean_flutter_app_updated/data/http/http.dart';
import 'package:clean_flutter_app_updated/data/usecases/usecases.dart';

import '../../../mocks/values_mocks.dart';
import '../../spy_mocks/spy_mocks.dart';

// Usar Mock para testar classes abstratas
// Triple A (Arrange/act/assert) Design Pattern

void main() {
  late RemoteAddAccount sut;
  late HttpClientSpy httpClient;
  late String url;
  late AddAccountParameters parameters;
  late Map apiResult;

  setUp(() {
    // sut = system under test
    url = faker.internet.httpsUrl();
    parameters = ParametersFactory.makeAddAccount();
    apiResult = ApiFactory.makeAccountJson();
    httpClient = HttpClientSpy();
    httpClient.mockRequest(apiResult);
    sut = RemoteAddAccount(httpClient: httpClient, url: url);
  });

  test('Should call HttpClient with correct values', () async {
    //act
    await sut.add(parameters);
    //assert
    verify(() => httpClient.request(
      url: url,
      method: 'post',
      body: {
        'name': parameters.name,
        'email': parameters.email,
        'password': parameters.password,
        'passwordConfirmation': parameters.passwordConfirmation,
      },
    ));
  });

  test('Should throw unexpected error if http client returns 400', () async {
    httpClient.mockRequestError(HttpError.badRequest);
    //act
    final future = sut.add(parameters);
    //assert
    expect(future, throwsA(DomainError.unexpected));
  });

  test('Should throw unexpected error if http client returns 404', () async {
    httpClient.mockRequestError(HttpError.notFound);
    //act
    final future = sut.add(parameters);
    //assert
    expect(future, throwsA(DomainError.unexpected));
  });

  test('Should throw unexpected error if http client returns 500', () async {
    httpClient.mockRequestError(HttpError.serverError);
    //act
    final future = sut.add(parameters);
    //assert
    expect(future, throwsA(DomainError.unexpected));
  });

  test('Should throw InvalidCredentialsError if http client returns 403',
      () async {
        httpClient.mockRequestError(HttpError.forbidden);
    //act
    final future = sut.add(parameters);
    //assert
    expect(future, throwsA(DomainError.emailInUse));
  });

  test('Should return an Account if http client returns 200', () async {
    //act
    final account = await sut.add(parameters);
    //assert
    expect(account.token, apiResult['accessToken']);
  });

  test(
      'Should throw unexpected error if http client returns 200 with invalid data',
      () async {
    httpClient.mockRequest({'invalid_key': 'invalid_value'});
    //act
    final future = sut.add(parameters);
    //assert
    expect(future, throwsA(DomainError.unexpected));
  });
}
