import 'package:flutter/material.dart';

import 'package:provider/provider.dart';

import '../../components/components.dart';
import '../../pages/signup/components/components.dart';
import '../../../ui/helpers/helpers.dart';
import '../../mixins/mixins.dart';

import 'package:clean_flutter_app_updated/ui/pages/signup/signup.dart';

class SignupPage extends StatelessWidget with KeyboardManager, LoadingManager, ErrorManager, NavigationManager{
  const SignupPage({required this.presenter, Key? key}) : super(key: key);
  final SignupPresenter presenter;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Builder(
        builder: (context) {
          handleLoading(context, presenter.isLoadingStream);
          handleError(context, presenter.mainErrorStream);
          handleNavigation(presenter.navigateToStream, clearNavigation: true);
          return GestureDetector(
            onTap: () => hideKeyboard(context),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  const LoginHeader(),
                  Headline1(
                    text: R.strings.signupTitleText,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(32.0),
                    child: ListenableProvider(
                      create: (_) => presenter,
                      child: Form(
                        child: Column(
                          children: [
                            const NameInput(),
                            const SizedBox(height: 12),
                            const EmailInput(),
                            const SizedBox(height: 12),
                            const PasswordInput(),
                            const SizedBox(height: 12),
                            const PasswordConfirmationInput(),
                            const SizedBox(height: 12),
                            const SignupButton(),
                            TextButton.icon(
                              onPressed: presenter.goToLogin,
                              icon: const Icon(Icons.exit_to_app),
                              label:
                                  Text(R.strings.signupGoToLoginPageTextButton),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
