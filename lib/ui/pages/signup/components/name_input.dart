import 'package:flutter/material.dart';

import 'package:provider/provider.dart';

import '../signup.dart';
import '../../../helpers/errors/errors.dart';
import '../../../helpers/i18n/i18n.dart';

class NameInput extends StatelessWidget {
  const NameInput({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final presenter = Provider.of<SignupPresenter>(context);
    return StreamBuilder<UIError?>(
        stream: presenter.nameErrorStream,
        builder: (context, snapshot) {
          return TextFormField(
            decoration: InputDecoration(
              prefixIcon: const Icon(Icons.person),
              filled: true,
              labelText: R.strings.signupNameLabelText,
              errorText:  snapshot.data?.description,
            ),
            keyboardType: TextInputType.name,
            onChanged: presenter.validateName,
          );
        });
  }
}
