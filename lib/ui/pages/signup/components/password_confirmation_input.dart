import 'package:flutter/material.dart';

import 'package:provider/provider.dart';

import '../signup.dart';
import '../../../helpers/errors/errors.dart';
import '../../../helpers/i18n/i18n.dart';

class PasswordConfirmationInput extends StatelessWidget {
  const PasswordConfirmationInput({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final presenter = Provider.of<SignupPresenter>(context);
    return StreamBuilder<UIError?>(
        stream: presenter.passwordConfirmationErrorStream,
        builder: (context, snapshot) {
          return TextFormField(
            decoration: InputDecoration(
              labelText: R.strings.signupPasswordConfirmationLabelText,
              filled: true,
              prefixIcon: const Icon(Icons.lock),
              errorText:  snapshot.data?.description,
            ),
            obscureText: true,
            onChanged: presenter.validatePasswordConfirmation,
          );
        });
  }
}
