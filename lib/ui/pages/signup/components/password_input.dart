import 'package:flutter/material.dart';

import 'package:provider/provider.dart';

import '../signup.dart';
import '../../../helpers/errors/errors.dart';
import '../../../helpers/i18n/i18n.dart';

class PasswordInput extends StatelessWidget {
  const PasswordInput({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final presenter = Provider.of<SignupPresenter>(context);
    return StreamBuilder<UIError?>(
        stream: presenter.passwordErrorStream,
        builder: (context, snapshot) {
          return TextFormField(
            decoration: InputDecoration(
              labelText: R.strings.signupPasswordLabelText,
              filled: true,
              prefixIcon: const Icon(Icons.lock),
              errorText:  snapshot.data?.description,
            ),
            obscureText: true,
            onChanged: presenter.validatePassword,
          );
        });
  }
}
