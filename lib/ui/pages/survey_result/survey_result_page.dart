import 'package:flutter/material.dart';

import '../../mixins/mixins.dart';
import '../../components/components.dart';
import '../../helpers/helpers.dart';
import 'components/components.dart';
import 'survey_result.dart';

class SurveyResultPage extends StatelessWidget with LoadingManager, SessionManager {
  final SurveyResultPresenter presenter;

  SurveyResultPage({Key? key, required this.presenter}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(R.strings.surveysAppBarTitle),
      ),
      body: Builder(
        builder: (context) {
          handleLoading(context, presenter.isLoadingStream);
          handleSession(presenter.isSessionExpiredStream);
          presenter.loadData();
          return StreamBuilder<SurveyResultViewModel?>(
              stream: presenter.surveyResultStream,
              builder: (context, snapshot) {
                if (snapshot.hasError) {
                  return ReloadScreen(
                      error: '${snapshot.error}', reload: presenter.loadData);
                } else if (snapshot.hasData) {
                  return SurveyResult(viewModel: snapshot.data!, onSave: presenter.save,);
                } else {
                  return Container();
                }
              });
        },
      ),
    );
  }
}
