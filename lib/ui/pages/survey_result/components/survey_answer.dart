import 'package:flutter/material.dart';

import '../survey_result.dart';
import 'components.dart';

class SurveyAnswer extends StatelessWidget {
  const SurveyAnswer(this.viewModel, {Key? key}) : super(key: key);

  final SurveyAnswerViewModel viewModel;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          padding: const EdgeInsets.all(16),
          decoration: BoxDecoration(
            color: Theme.of(context).backgroundColor,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              viewModel.image != null
                  ? Image.network(
                viewModel.image!,
                width: 40,
              )
                  : Container(),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 12.0),
                  child: Text(
                    viewModel.answer,
                    style: const TextStyle(
                      fontSize: 16,
                    ),
                  ),
                ),
              ),
              Text(
                viewModel.percent,
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                  color: Theme.of(context).primaryColorDark,
                ),
              ),
              viewModel.isCurrentAnswer
                  ? const ActiveIcon()
                  : const DisabledIcon(),
            ],
          ),
        ),
        const Divider(
          height: 1,
        ),
      ],
    );
  }
}

