import 'package:flutter/material.dart';

import 'package:provider/provider.dart';

import '../login_presenter.dart';
import '../../../../ui/helpers/i18n/i18n.dart';
import '../../../helpers/errors/errors.dart';

class EmailInput extends StatelessWidget {
  const EmailInput({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final presenter = Provider.of<LoginPresenter>(context);
    return StreamBuilder<UIError?>(
        stream: presenter.emailErrorStream,
        builder: (context, snapshot) {
          return TextFormField(
            decoration: InputDecoration(
              prefixIcon: const Icon(Icons.email),
              filled: true,
              labelText: R.strings.loginEmailLabelText,
              errorText: snapshot.data?.description,
            ),
            keyboardType: TextInputType.emailAddress,
            onChanged: presenter.validateEmail,
          );
        });
  }
}
