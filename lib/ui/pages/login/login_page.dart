import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../ui/helpers/helpers.dart';
import '../../components/components.dart';
import './login_presenter.dart';
import './components/components.dart';
import '../../mixins/mixins.dart';

class LoginPage extends StatelessWidget with KeyboardManager, LoadingManager, ErrorManager, NavigationManager {
  const LoginPage({required this.presenter, Key? key}) : super(key: key);
  final LoginPresenter presenter;

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: Builder(
        builder: (context) {
          handleLoading(context, presenter.isLoadingStream);
          handleError(context, presenter.mainErrorStream);
          handleNavigation(presenter.navigateToStream, clearNavigation: true);
          return GestureDetector(
            onTap: () => hideKeyboard(context),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  const LoginHeader(),
                  Headline1(
                    text: R.strings.loginTitleText,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(32.0),
                    child: ListenableProvider(
                      create: (_) => presenter,
                      child: Form(
                        child: Column(
                          children: [
                            const EmailInput(),
                            const SizedBox(height: 12),
                            const PasswordInput(),
                            const SizedBox(height: 12),
                            const LoginButton(),
                            TextButton.icon(
                              onPressed: presenter.goToSignUp,
                              icon: const Icon(Icons.person),
                              label: Text(
                                  R.strings.loginGoToCreateAccountButtonText),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
