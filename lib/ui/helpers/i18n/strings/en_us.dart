import 'translations.dart';

class EnUs implements Translations {
  //Login page translations
  @override
  String get loadingDialogText => 'Loading...';
  @override
  String get loginButtonText => 'Login';
  @override
  String get loginGoToCreateAccountButtonText => 'Create account';
  @override
  String get loginEmailLabelText => 'Email';
  @override
  String get loginTitleText => 'Login';
  @override
  String get loginPasswordLabelText => 'Password';

  //Signup page translations
  @override
  String get signupNameLabelText => 'Name';
  @override
  String get signupEmailLabelText => 'Email';
  @override
  String get signupPasswordLabelText => 'Password';
  @override
  String get signupPasswordConfirmationLabelText => 'Password Confirmation';
  @override
  String get signupGoToLoginPageTextButton =>
      'Already have an account? Log in here!';

  @override
  String get signupCreateAccountButtonText => 'Create account';
  @override
  String get signupTitleText => 'Sign Up';

  //Surveys page translations
  @override
  String get surveysAppBarTitle => 'Surveys';
  @override
  String get surveysPageReload => 'Reload';

  //Errors translations
  @override
  String get credentialsError => 'Invalid email or password. Try again.';
  @override
  String get emailInUseError => 'Email already in use!';
  @override
  String get unexpectedError => 'An unexpected error occurred. Try again later.';

  //LoginValidation errors
  @override
  String get invalidFieldError => 'Invalid field.';
  @override
  String get requiredFieldError => 'Required field.';
}
