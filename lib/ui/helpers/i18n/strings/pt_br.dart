import 'translations.dart';

class PtBr implements Translations {
  //Login page translations
  @override
  String get loginGoToCreateAccountButtonText => 'Criar conta';
  @override
  String get loginButtonText => 'Entrar';
  @override
  String get loginEmailLabelText => 'Email';
  @override
  String get loginPasswordLabelText => 'Senha';
  @override
  String get loginTitleText => 'Login';
  @override
  String get loadingDialogText => 'Carregando...';

  //Signup page translations
  @override
  String get signupNameLabelText => 'Nome';
  @override
  String get signupEmailLabelText => 'Email';
  @override
  String get signupPasswordLabelText => 'Senha';
  @override
  String get signupPasswordConfirmationLabelText => 'Confirmação da senha';
  @override
  String get signupGoToLoginPageTextButton => 'Já tem uma conta? Entre aqui!';
  @override
  String get signupCreateAccountButtonText => 'Criar conta';
  @override
  String get signupTitleText => 'Cadastro';

  //Surveys page translations
  @override
  String get surveysAppBarTitle => 'Enquetes';
  @override
  String get surveysPageReload => 'Recarregar';

  //Errors translations
  @override
  String get credentialsError => 'Email ou senha inválidos. Tente novamente.';
  @override
  String get emailInUseError => 'Este email já está em uso!';
  @override
  String get unexpectedError => 'Erro inesperado. Tente novamente mais tarde';

  //LoginValidation errors
  @override
  String get invalidFieldError => 'Campo inválido.';
  @override
  String get requiredFieldError => 'Campo obrigatório.';
}
