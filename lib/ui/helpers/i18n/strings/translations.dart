abstract class Translations {
  // Manter as Strings em ordem alfabética
  //Login page translations
  String get loadingDialogText;
  String get loginButtonText;
  String get loginEmailLabelText;
  String get loginGoToCreateAccountButtonText;
  String get loginPasswordLabelText;
  String get loginTitleText;

  //Signup page translations
  String get signupCreateAccountButtonText;
  String get signupEmailLabelText;
  String get signupGoToLoginPageTextButton;
  String get signupNameLabelText;
  String get signupPasswordLabelText;
  String get signupPasswordConfirmationLabelText;
  String get signupTitleText;

  //Surveys page translations
  String get surveysAppBarTitle;
  String get surveysPageReload;

  //Errors translations
  String get credentialsError;
  String get emailInUseError;
  String get unexpectedError;

  //LoginValidation errors
  String get invalidFieldError;
  String get requiredFieldError;
}
