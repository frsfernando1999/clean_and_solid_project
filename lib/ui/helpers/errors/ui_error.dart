import '../../../ui/helpers/i18n/i18n.dart';

enum UIError {
  requiredField,
  invalidField,
  unexpected,
  invalidCredentials,
  emailInUse,
}

extension UIErrorExtension on UIError {
  String get description {
    switch (this) {
      case UIError.requiredField:
        return R.strings.requiredFieldError;

      case UIError.invalidField:
        return R.strings.invalidFieldError;

      case UIError.invalidCredentials:
        return R.strings.credentialsError;

      case UIError.emailInUse:
        return R.strings.emailInUseError;

      case UIError.unexpected:
      default:
        return R.strings.unexpectedError;
    }
  }
}
