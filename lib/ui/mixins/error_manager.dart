import 'package:flutter/material.dart';

import '../../ui/components/components.dart';
import '../../ui/helpers/helpers.dart';

mixin ErrorManager {
  void handleError(BuildContext context, Stream<UIError?> stream) {
    stream.listen((error) {
      if (error != null) {
        showErrorSnackbar(context, error.description);
      }
    });
  }
}
