import 'package:get/route_manager.dart';

mixin NavigationManager {
  void handleNavigation(Stream<String?> stream, {bool clearNavigation = false}) {
    stream.listen((page) {
      if (page != null && page.isNotEmpty) {
        if(clearNavigation == true){
        Get.offAllNamed(page);
        }else{
          Get.toNamed(page);
        }
      }
    });
  }
}
