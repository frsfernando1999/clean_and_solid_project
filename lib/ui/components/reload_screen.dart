import 'package:flutter/material.dart';

import '../helpers/helpers.dart';

class ReloadScreen extends StatelessWidget {

  final String error;
  final Future<void> Function() reload;

  const ReloadScreen({Key? key, required this.error, required this.reload}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            error,
            style: const TextStyle(
              fontSize: 16,
            ),
            textAlign: TextAlign.center,
          ),
          const SizedBox(
            height: 12,
          ),
          ElevatedButton(
            onPressed: reload,
            child: Text(R.strings.surveysPageReload),
          )
        ],
      ),
    );
  }
}