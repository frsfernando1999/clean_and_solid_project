import '../../../domain/usecases/usecases.dart';
import '../../../domain/helpers/helpers.dart';
import '../../../domain/entities/entities.dart';

import '../../http/http.dart';
import '../../models/models.dart';

class RemoteAuthentication implements Authentication {
  final HttpClient httpClient;
  final String url;

  RemoteAuthentication({
    required this.httpClient,
    required this.url,
  });

  @override
  Future<AccountEntity> auth(AuthenticationParameters parameters) async {
    try {
      final httpResponse = await httpClient.request(
        url: url,
        method: 'post',
        body: RemoteAuthenticationParameters.fromDomain(parameters).toJson(),
      );
      return RemoteAccountModel.fromJson(httpResponse).toEntity();
    } on HttpError catch (error) {
      throw error == HttpError.unauthorized
          ? DomainError.invalidCredentials
          : DomainError.unexpected;
    }
  }
}

class RemoteAuthenticationParameters {
  final String email;
  final String password;

  const RemoteAuthenticationParameters({
    required this.email,
    required this.password,
  });

  factory RemoteAuthenticationParameters.fromDomain(
          AuthenticationParameters parameters) =>
      RemoteAuthenticationParameters(
          email: parameters.email, password: parameters.secret);

  Map toJson() => {'email': email, 'password': password};
}
