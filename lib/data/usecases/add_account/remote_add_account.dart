import 'package:clean_flutter_app_updated/data/models/models.dart';


import '../../../domain/helpers/helpers.dart';
import '../../../domain/usecases/usecases.dart';
import '../../../domain/entities/entities.dart';

import '../../http/http.dart';

class RemoteAddAccount implements AddAccount {
  final HttpClient httpClient;
  final String url;

  RemoteAddAccount({
    required this.httpClient,
    required this.url,
  });

  @override
  Future<AccountEntity> add(AddAccountParameters parameters) async {
    try {
      final httpResponse = await httpClient.request(
        url: url,
        method: 'post',
        body: RemoteAddAccountParameters.fromDomain(parameters).toJson(),
      );
      return RemoteAccountModel.fromJson(httpResponse).toEntity();
    } on HttpError catch (error) {
      throw error == HttpError.forbidden
          ? DomainError.emailInUse
          : DomainError.unexpected;
    }
  }
}

class RemoteAddAccountParameters {
  final String name;
  final String email;
  final String password;
  final String passwordConfirmation;

  RemoteAddAccountParameters({
    required this.name,
    required this.email,
    required this.password,
    required this.passwordConfirmation,
  });

  factory RemoteAddAccountParameters.fromDomain(
          AddAccountParameters parameters) =>
      RemoteAddAccountParameters(
        name: parameters.name,
        email: parameters.email,
        password: parameters.password,
        passwordConfirmation: parameters.passwordConfirmation,
      );

  Map toJson() => {
        'name': name,
        'email': email,
        'password': password,
        'passwordConfirmation': passwordConfirmation,
      };
}
