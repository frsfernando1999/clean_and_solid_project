import 'package:get/get.dart';
import '../../ui/helpers/helpers.dart';

mixin PasswordErrorManager on GetxController{
  var passwordError = Rx<UIError?>(null);

  Stream<UIError?> get passwordErrorStream => passwordError.stream;

  set setPasswordError(UIError? value) => passwordError.value = value;
}
