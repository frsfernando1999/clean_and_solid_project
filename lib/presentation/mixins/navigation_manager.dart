import 'package:get/get.dart';

mixin NavigationManager on GetxController{
  final _navigateTo = Rx<String?>(null);

  Stream<String?> get navigateToStream => _navigateTo.stream;
  // subject.add permite uma stream receber dois valores iguais em seguida
  set navigateTo(String value) => _navigateTo.subject.add(value);
}
