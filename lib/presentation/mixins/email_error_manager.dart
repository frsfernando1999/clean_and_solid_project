import 'package:get/get.dart';
import '../../ui/helpers/helpers.dart';

mixin EmailErrorManager on GetxController {
  var emailError = Rx<UIError?>(null);

  Stream<UIError?> get emailErrorStream => emailError.stream;

  set setEmailError(UIError? value) => emailError.value = value;
}
