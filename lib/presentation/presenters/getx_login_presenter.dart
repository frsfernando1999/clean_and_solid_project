
import 'package:get/get.dart';

import '../../ui/pages/login/login_presenter.dart';
import '../../ui/helpers/errors/errors.dart';

import '../../domain/usecases/save_current_account.dart';
import '../../domain/helpers/domain_error.dart';
import '../../domain/usecases/authentication.dart';

import '../protocols/protocols.dart';
import '../mixins/mixins.dart';

class GetxLoginPresenter extends GetxController
    with
        LoadingManager,
        NavigationManager,
        EmailErrorManager,
        PasswordErrorManager,
        ErrorManager,
        FormValidManager
    implements LoginPresenter {
  GetxLoginPresenter({
    required this.validation,
    required this.authentication,
    required this.saveCurrentAccount,
  });

  final Authentication authentication;
  final Validation validation;
  final SaveCurrentAccount saveCurrentAccount;

  String? _email;
  String? _password;

  @override
  void validateEmail(String email) {
    _email = email;
    setEmailError = _validateField('email');
    _validateForm();
  }

  @override
  void validatePassword(String password) {
    _password = password;

    setPasswordError = _validateField('password');
    _validateForm();
  }

  UIError? _validateField(String field) {
    final formData = {
      'email': _email,
      'password': _password,
    };
    final error = validation.validate(field: field, input: formData);
    switch (error) {
      case ValidationError.invalidField:
        return UIError.invalidField;
      case ValidationError.requiredField:
        return UIError.requiredField;
      default:
        return null;
    }
  }

  void _validateForm() {
    isFormValid = emailError.value == null &&
        passwordError.value == null &&
        _email != null &&
        _password != null;
  }

  @override
  Future<void> auth() async {
    try {
      mainError = null;
      isLoading = true;
      final account = await authentication.auth(AuthenticationParameters(
        email: _email!,
        secret: _password!,
      ));
      await saveCurrentAccount.save(account);
      navigateTo = '/surveys';
    } on DomainError catch (error) {
      switch (error) {
        case DomainError.invalidCredentials:
          mainError = UIError.invalidCredentials;
          break;
        default:
          mainError = UIError.unexpected;
      }
      isLoading = false;
    }
  }

  @override
  void goToSignUp() {
    navigateTo = '/signup';
  }
}
