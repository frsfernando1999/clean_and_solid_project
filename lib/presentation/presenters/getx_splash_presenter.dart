

import 'package:get/get.dart';

import '../../presentation/mixins/mixins.dart';

import '../../domain/usecases/usecases.dart';

import '../../ui/pages/pages.dart';

class GetxSplashPresenter extends GetxController with NavigationManager implements SplashPresenter {
  final LoadCurrentAccount loadCurrentAccount;

  GetxSplashPresenter({required this.loadCurrentAccount});

  @override
  Future<void> checkCurrentAccount({int durationInMilliseconds = 1500}) async {
    await Future.delayed(Duration(milliseconds: durationInMilliseconds));
    try {
      await loadCurrentAccount.load();
      navigateTo =  '/surveys';
    } catch (error) {
      navigateTo = '/login';
    }
  }
}
