import 'package:get/get.dart';

import '../../domain/helpers/helpers.dart';
import '../../domain/usecases/usecases.dart';

import '../../ui/helpers/errors/errors.dart';

import '../protocols/protocols.dart';
import '../../presentation/mixins/mixins.dart';

import '../../ui/pages/pages.dart';

class GetxSignupPresenter extends GetxController
    with
        LoadingManager,
        NavigationManager,
        PasswordErrorManager,
        EmailErrorManager,
        ErrorManager,
        FormValidManager
    implements SignupPresenter {
  GetxSignupPresenter(
      {required this.validation,
      required this.addAccount,
      required this.saveCurrentAccount});

  final Validation validation;
  final AddAccount addAccount;
  final SaveCurrentAccount saveCurrentAccount;

  final _nameError = Rx<UIError?>(null);
  final _passwordConfirmationError = Rx<UIError?>(null);


  String? _name;
  String? _email;
  String? _password;
  String? _passwordConfirmation;

  @override
  Stream<UIError?> get nameErrorStream => _nameError.stream;

  @override
  Stream<UIError?> get passwordConfirmationErrorStream =>
      _passwordConfirmationError.stream;

  @override
  void validateEmail(String email) {
    _email = email;
    setEmailError = _validateField('email');
    _validateForm();
  }

  @override
  void validateName(String name) {
    _name = name;
    _nameError.value = _validateField('name');
    _validateForm();
  }

  @override
  void validatePassword(String password) {
    _password = password;
    setPasswordError = _validateField('password');
    _validateForm();
  }

  @override
  void validatePasswordConfirmation(String passwordConfirmation) {
    _passwordConfirmation = passwordConfirmation;
    _passwordConfirmationError.value = _validateField('passwordConfirmation');
    _validateForm();
  }


  UIError? _validateField(String field) {
    final formData = {
      'email': _email,
      'name': _name,
      'password': _password,
      'passwordConfirmation': _passwordConfirmation,
    };

    final error = validation.validate(field: field, input: formData);
    switch (error) {
      case ValidationError.invalidField:
        return UIError.invalidField;
      case ValidationError.requiredField:
        return UIError.requiredField;
      default:
        return null;
    }
  }

  void _validateForm() {
    isFormValid = emailError.value == null &&
        _nameError.value == null &&
        passwordError.value == null &&
        _passwordConfirmationError.value == null &&
        _email != null &&
        _name != null &&
        _passwordConfirmation != null &&
        _password != null;
  }

  @override
  Future<void> signup() async {
    try {
      mainError = null;
      isLoading = true;
      final account = await addAccount.add(AddAccountParameters(
        name: _name!,
        email: _email!,
        password: _password!,
        passwordConfirmation: _passwordConfirmation!,
      ));
      await saveCurrentAccount.save(account);
      navigateTo = '/surveys';
    } on DomainError catch (error) {
      switch (error) {
        case DomainError.emailInUse:
          mainError = UIError.emailInUse;
          break;
        default:
          mainError = UIError.unexpected;
      }
      isLoading = false;
    }
  }

  @override
  void goToLogin() {
    navigateTo = '/login';
  }
}
