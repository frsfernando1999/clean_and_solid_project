import '../../../data/usecases/usecases.dart';
import '../../../domain/usecases/usecases.dart';
import '../../factories/factories.dart';

LoadCurrentAccount makeLocalLoadCurrentAccount() {
  return LocalLoadCurrentAccount(
      fetchSecureCacheStorage: makeSecureStorageAdapter());
}
