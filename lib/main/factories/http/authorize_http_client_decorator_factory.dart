import '../../../data/http/http.dart';

import '../../decorators/decorators.dart';
import '../../factories/factories.dart';

HttpClient makeAuthorizeHttpClientDecorator() => AuthorizeHttpClientDecorator(
    deleteSecureCacheStorage: makeSecureStorageAdapter(),
      fetchSecureCacheStorage: makeSecureStorageAdapter(),
      decoratee: makeHttpAdapter() ,
    );
