import '../../../../ui/pages/survey_result/survey_result.dart';
import '../../../../presentation/presenters/presenters.dart';
import '../../factories.dart';

SurveyResultPresenter makeGetxSurveyResultPresenter(String surveyId) {
  return GetXSurveyResultPresenter(
    saveSurveyResult: makeRemoteSaveSurveyResult(surveyId),
    loadSurveyResult: makeRemoteLoadSurveyResultWithLocalFallback(surveyId),
    surveyId: surveyId,
  );
}
