import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../../../ui/pages/pages.dart';
import '../pages.dart';


Widget makeSurveyResultPage() => SurveyResultPage(
      presenter: makeGetxSurveyResultPresenter(Get.parameters['survey_id'] ?? ''),
    );
