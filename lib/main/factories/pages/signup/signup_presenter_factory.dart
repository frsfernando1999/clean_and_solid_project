import '../../factories.dart';
import '../../../../ui/pages/pages.dart';
import '../../../../presentation/presenters/presenters.dart';

SignupPresenter makeGetxSignupPresenter() {
  return GetxSignupPresenter(
    addAccount: makeRemoteAddAccount(),
    validation: makeSignupValidation(),
    saveCurrentAccount: makeLocalSaveCurrentAccount(),
  );
}
