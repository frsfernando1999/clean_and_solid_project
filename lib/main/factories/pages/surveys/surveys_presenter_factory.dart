import '../../../../ui/pages/pages.dart';
import '../../../../presentation/presenters/presenters.dart';
import '../../factories.dart';

SurveysPresenter makeGetxSurveysPresenter() {
  return GetXSurveysPresenter(
    loadSurveys: makeRemoteLoadSurveysWithLocalFallback(),
  );
}
