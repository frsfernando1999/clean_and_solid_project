import '../../../../validation/protocols/protocols.dart';
import '../../../../presentation/protocols/protocols.dart';
import '../../../composites/composites.dart';
import '../../../builders/builders.dart';

Validation makeLoginValidations() {
  return ValidationComposite(makeLoginValidation());
}

List<FieldValidation> makeLoginValidation() {
  return [
    ...ValidationBuilder.field('email').required().email().build(),
    ...ValidationBuilder.field('password').required().minLength(3).build(),
  ];
}
