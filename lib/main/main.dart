import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

import '../ui/helpers/i18n/i18n.dart';
import '../ui/components/components.dart';
import './factories/factories.dart';

//This is the composition root design pattern

void main() {
  if (Platform.localeName == 'pt_BR' || Platform.localeName == 'en_US') {
    R.load(Locale(Platform.localeName));
  } else {
    R.load(const Locale('en_US'));
  }
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light);
    final routeObserver = Get.put<RouteObserver>(RouteObserver<PageRoute>());

    return GetMaterialApp(
      title: '4Dev Tutorial',
      debugShowCheckedModeBanner: false,
      theme: makeAppTheme(),
      navigatorObservers: [
      routeObserver
      ],
      initialRoute: '/',
      getPages: [
        GetPage(
          name: '/',
          page: makeSplashPage,
          transition: Transition.fade,
        ),
        GetPage(
          name: '/login',
          page: makeLoginPage,
          transition: Transition.fadeIn,
        ),
        GetPage(
          name: '/signup',
          page: makeSignupPage,
        ),
        GetPage(
            name: '/surveys',
            page: makeSurveysPage,
            transition: Transition.fadeIn),
        GetPage(name: '/survey_result/:survey_id', page: makeSurveyResultPage),
      ],
    );
  }
}
