import 'package:equatable/equatable.dart';

import '../protocols/protocols.dart';

import '../../presentation/protocols/protocols.dart';

class RequiredFieldValidation extends Equatable implements FieldValidation {
  const RequiredFieldValidation(this.field);

  @override
  final String field;

  @override
  ValidationError? validate(Map input) {
    return input[field]?.isNotEmpty == true
        ? null
        : ValidationError.requiredField;
  }

  @override
  List<Object> get props => [field];
}
