import 'package:equatable/equatable.dart';


import 'package:clean_flutter_app_updated/presentation/protocols/protocols.dart';
import 'package:clean_flutter_app_updated/validation/protocols/protocols.dart';

class CompareFieldsValidation extends Equatable implements FieldValidation {
  @override
  final String field;
  final String fieldToCompare;

  const CompareFieldsValidation(
      {required this.field, required this.fieldToCompare});

  @override
  ValidationError? validate(Map input) {
    return input[field] != null &&
            input[fieldToCompare] != null &&
            input[field] != input[fieldToCompare]
        ? ValidationError.invalidField
        : null;
  }
  @override
  List<Object> get props => [field, fieldToCompare];

}
