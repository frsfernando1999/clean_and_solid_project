import 'package:equatable/equatable.dart';

import '../entities/entities.dart';

abstract class AddAccount {
  Future<AccountEntity> add(AddAccountParameters parameters);
}

class AddAccountParameters extends Equatable {
  final String name;
  final String email;
  final String password;
  final String passwordConfirmation;

  @override
  List get props => [name, email, password, passwordConfirmation];

  const AddAccountParameters({
    required this.name,
    required this.email,
    required this.password,
    required this.passwordConfirmation,
  });
}
