# Updated Clean Flutter App

## What is this project about?

- This project was used to study the main concepts of clean architecture, design patterns and testing.

## What does that mean?

The whole application was built with code decoupling in mind, separating the UI, Presentation, Usecases, Data and Infrastructure in layers.

This way, all the project parts are like jigsaw pieces, and the application works by putting its pieces together.

All the functionalities use abstract classes (interfaces), so that no logic is left out or forgotten. Since the classes depend on these interfaces, any changes made that obey the interface rules and pass the tests will guarantee that the project still works normally

## What about testing?

The classes and it's methods were created and being tested as they were being developed. 

### How does that work?
1. You create a class to be tested, initially, you don't put any logic in it.
2. You create a test for that class (Ex.: Calling a function)
3. Make it fail. It's important to test this first to guarantee that a test fails when it should.
4. Put the logic in the class to make the test pass and run it again (Always try to write the minimum amount of code to make the a specific test pass).
5. If you aren't writing the first test, check if the logic you wrote broke another test and fix it.
6. Once you tested and implemented everything it should do, make it implement it's pre-supposed interface

## What do you mean by jigsaw puzzle?

The code is decoupled, so, most classes depend on others. 
When using the factory design pattern, you'll be following the intellisense warnings and errors to add the dependencies the classes needs until it's warnings disappears. 
Once all dependencies are in place, the project is safe to run.

