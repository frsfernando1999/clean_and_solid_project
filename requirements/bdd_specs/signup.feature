Feature: Criar Conta
  Como um cliente
  Quero poder criar uma conta e me manter logado
  Para que eu possa ver e responder enquetes de forma rápida

  Scenario: Dados Válidos
  Given: Dado que o cliente informou dados válidos
  When: Quando solicitar para criar a conta
  Then: Então o sistema deve enviar o usuário para a tela de pesquisas
  And: E manter o usuário conectado

  Scenario: Dados Inválidos
  Given: Dado que o cliente informou dados inválidos
  When: Quando solicitar para criar a conta
  Then: Então o sistema deve retornar uma mensagem de erro