Feature: Resultado de uma Enquete
  Como um cliente
  Quero poder ver o resultado de uma enquete
  Para saber a opinião da comunidade a respeito de cada tópico

  Scenario: Com internet
  Given: Dado que o cliente tem conexão com a internet
  When:  Quando solicitar para ver o resultado de uma enquete
  Then:  Então o sistema deve exibir o resultado da enquete
  And: E armazenar os dados atualizados no cache

  Scenario: Sem internet
  Given: Dado que o cliente não tem conexão com a internet
  When: Quando solicitar para ver o resultado de uma enquete
  Then: Então o sistema deve exibir o resultado da enquete que foi gravado no cache no último acesso
