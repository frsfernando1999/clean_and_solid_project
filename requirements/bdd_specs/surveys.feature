Feature: Listar Enquetes
  Como um cliente
  Quero poder ver todas as enquetes
  Para poder saber o resultado e poder dar minha opinião

  Scenario: Com internet
  Given: Dado que o cliente tem conexão com a internet
  When: Quando solicitar para ver as enquetes
  Then: Então o sistema deve exibir as enquetes
  And: E armazenar os dados atualizados no cache

  Scenario: Sem internet
  When: Dado que o cliente não tem conexão com a internet
  Then: Quando solicitar para ver as enquetes
  Então o sistema deve exibir as enquetes que foram gravadas no cache no último acesso