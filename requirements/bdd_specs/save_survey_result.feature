Feature: Responder uma Enquete
  Como um cliente
  Quero poder responder uma enquete
  Para dar minha contribuição e opinião para a comunidade

  Scenario: Com internet
  Given: Dado que o cliente tem conexão com a internet
    When:  quando solicitar para responder uma enquete
    Then:  então o sistema deve gravar sua resposta
    And: e atualizar o cache com as novas estatísticas
    And: e mostrar para o usuário o resultado atualizado

  Scenario: Sem internet
    Given: Dado que o cliente não tem conexão com a internet
    When:  quando solicitar para responder uma enquete
    Then:  então o sistema deve exibir uma mensagem de erro