# Survey Result Page

> ## Regras
1. ✔ Chamar o método de Obter Resultado da Enquete ao carregar tela
2. ✔ Exibir loading ao receber evento de isLoading do presenter como true
3. ✔ Esconder loading ao receber evento de isLoading do presenter como false ou null
4. ✔ Exibir mensagem de erro e esconder a lista ao receber evento de surveyResult com erro
5. ✔ Chamar o método de obter resultado da enquete ao clicar no botão de recarregar
6. ✔ Esconder mensagem de erro e exibir o resultado da enquete ao receber evento de surveyResult com dados
7. ✔ Exibir as perguntas da enquete
8. ✔ Exibir as respostas da enquete
9. ✔ Exibir a porcentagem de cada resposta
10. ✔ Exibir o ícone desativado se não for a resposta do usuário
11. ✔ Exibir o ícone ativado se for a resposta correta do usuário
12. ✔ Carregar a imagem a partir da URL correta, caso a resposta tenha imagem
13. ✔ Não renderizar imagem, caso a resposta não tenha imagem
14. ✔ Ir para a tela de Login e limpar a navegação ao receber evento de sessionExpired como true
15. Chamar o método de Salvar Resultado da Enquete ao clicar em algum item da lista