# Remote Authentication Use Case
> ## Caso de sucesso
1. ✔ Sistema valida os dados
2. ✔ Sistema faz uma requisição para a URL da API de login
3. ✔ Sistema Valida os dados recebidos da API
4. ✔ Sistema Valida os dados da conta do usuário

> ## Exceção - URL inválida
1. ✔ Sistema retorna uma mensagem de erro inesperado

> ##Exceção - Dados inválidos
1. ✔ Sistema retorna uma mensagem de erro inesperado 

> ##Exceção - Resposta Inválida
1. ✔ Sistema retorna uma mensagem de erro inesperado 

> ##Exceção - Falha no Servidor
1. ✔ Sistema retorna uma mensagem de erro inesperado 

> ##Exceção - Credenciais inválidas
1. ✔ Sistema retorna uma mensagem de erro informando que as credenciais estão erradas
